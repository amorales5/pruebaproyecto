<?php

namespace App\ModelosSoporte;

use Illuminate\Database\Eloquent\Model;

class SummaryTicket extends Model
{
    protected $table = "v_cc_casos";
    public $timestamps = false;
    protected $fillable = [
        'id_caso', 'clave_cuenta', 'titulo','descripcion','comentarios','fecha_creacion','tipocaso','prioridad','asignado_a'
    ];
}
