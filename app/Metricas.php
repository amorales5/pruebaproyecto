<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Metricas extends Model
{
    protected $table = "v_hc_colaboramatrices";
    public $timestamps = false;
    protected $fillable = [
        'id_matriz','id_colaborador', 'nombre_colaboradr', 'nombre','puesto','area','fecha_ini','fecha_fin','mes','anio','observaciones','resultado','cerrada','codigo_unidad'
    ];
}
