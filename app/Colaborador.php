<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Colaborador extends Model
{
    protected $table = "v_hc_colaboradores";
    public $timestamps = false;
    protected $fillable = [
        'id_colaborador', 'nombre', 'correo_trabajo','ingreso_fecha','puesto','area','supervisor','telefono_movil','id_area'
    ];
}
