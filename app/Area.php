<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Area extends Model
{
    protected $table = "v_hc_areas";
    public $timestamps = false;
    protected $fillable = [
        'id_area','nombre', 'id_tipoarea', 'id_responsable','codigo_unidad','activa'
    ];
}
