<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Unidad extends Model
{
    protected $table = "efi_unidnegocio";
    public $timestamps = false;
    protected $fillable = [
        'codigo_unidad','codigo_tipounidd','descripcion', 'imagen'
    ];
}
