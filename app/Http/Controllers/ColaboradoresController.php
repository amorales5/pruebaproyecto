<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\Resource;

use Yajra\DataTables\DataTables;
use App\Empleado;
use App\User;
use App\Unidad;
use App\Metricas;
use App\Area;
//use APP\VColaboradores;

class ColaboradoresController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $filtros['unidades'] = Unidad::where('codigo_tipounidad','<>','UNS')->get()->toArray();
        $filtros['areas'] = Area::where('activa','=','True')->get()->toArray();
       
        //$unidades = Unidad::all(); User::where("estado","=",1)->get()->toJson();
        return view('rh.colaboradores')->with('filtros', $filtros);
    }
    public function colaboraFiltro(Request $request){
        //var_dump($request->un);
       // echo $request->un;
        //exit();
        $wheresClaus = [];
        $unidades = $request->un;
        //print_r($unidades);
        if($unidades!='Todos'){
            array_push($wheresClaus,['un_codigo','=',$unidades]);
        }
        if($request->area!=0){
            array_push($wheresClaus,['id_area','=',$request->area]);
        }
         if($request->estatus<3){
             array_push($wheresClaus,['status','=',$request->estatus]);
        }
        if($request->ingreso!=0){
                switch ($request->ingreso) {
                    case '1':
                        $dias = 11;
                        array_push($wheresClaus,['mesesingreso','<',$dias]);
                        break;
                     case '2':
                        $entre = 11;
                        array_push($wheresClaus,['mesesingreso','>=',12]);
                        array_push($wheresClaus,['mesesingreso','<',25]);
                        break;   
                     case '3':
                        array_push($wheresClaus,['mesesingreso','>=',24]);
                        array_push($wheresClaus,['mesesingreso','<=',60]);
                        break;
                     case '4':
                        array_push($wheresClaus,['mesesingreso','>=',60]);
                        array_push($wheresClaus,['mesesingreso','<=',160]);
                        break;     
                     case '5':
                        array_push($wheresClaus,['mesesingreso','>=',160]);
                        array_push($wheresClaus,['mesesingreso','<=',180]);
                        break;   
                    case '6':
                        array_push($wheresClaus,['mesesingreso','>',180]);
                        break;                    
                    default:
                        # code...
                        break;
                }

     
            //array_push($wheresClaus,['mesesingreso','<',$dias]);
        }

        $colaboradores = Empleado::where($wheresClaus)->get()->toArray();
      
        $colaboradores2 = array();
        //echo json_encode($colaboradores);
        //exit();

        foreach ($colaboradores as $key => $value) {
            //echo $value.'<br>';
            $a = route('empleados.show',$value['id_colaborador']);


            if($value['usuario_cognito']!=null){
                $file = "dist/img/colabora/".$value['usuario_cognito'].".png";
                $file2 = "dist/img/colabora/".$value['usuario_cognito'].".jpg";
                $exists = file_exists( $file );
                $exists2 = file_exists( $file2 );
                if($exists){
                    $im = $value['usuario_cognito'].'.png';
                }
                if($exists2){
                    $im = $value['usuario_cognito'].'.jpg';
                }
                if(!$exists2 && !$exists){
                    $im = 'sinfoto.png';
                }
            }else{
                 $im = 'sinfoto.png';
            }
            $colaboradores[$key]['nombre'] = "<div><div style='float: left;'><img src='dist/img/colabora/".$im."' style='border-radius: 50%; width: 40px;'></div><div style='float: left;margin-left:4px;'><a href='".route('empleados.show',$value['id_colaborador'])."'>".$value['nombre']."</a><br>".$value['puesto'].'</div></div>';
            $colaboradores[$key]['id_colaborador'] = "<a href='".route('empleados.show',$value['id_colaborador'])."'>".$value['id_colaborador']."</a>";
            $colaboradores[$key]['correo_trabajo'] = $value['correo_trabajo'];
            $colaboradores[$key]['ingreso_fecha'] = date_format(date_create($value['ingreso_fecha']), 'Y-m-d');
            $colaboradores[$key]['area'] = $value['area'];
            $colaboradores[$key]['puesto'] = $value['puesto'];
            $colaboradores[$key]['supervisor'] = $value['supervisor'];
            $colaboradores[$key]['telefono_movil'] = $value['telefono_movil'];
            
            $colaboradores[$key]['un_imagen'] = "<img src='".$value['un_imagen']."' style='width:50px; height:20px;' alt='".$value['un_codigo']."'>";

        }
       
        return DataTables::of($colaboradores)->addColumn('acciones','rh.acciones')->rawColumns(['acciones','nombre','id_colaborador','correo_trabajo','ingreso_fecha','area','puesto','supervisor','telefono_movil','un_imagen'])->make(true);
    }
    public function getColaboradores(){
       
        // $colaboradores = Empleado::whereNotNull('id_colaborador');
         //dd($colaboradores);
        //$colaboradores = Empleado::all();
        
        $colaboradores = Empleado::where("status","=",1)->get()->toArray();
      
        $colaboradores2 = array();
        // echo json_encode($colaboradores);
        // exit();

        foreach ($colaboradores as $key => $value) {
            //echo $value.'<br>';
            $a = route('empleados.show',$value['id_colaborador']);

           /* $col->nombre = "<a href='".route('empleados.show',$value['id_colaborador'])."'>".$value['nombre']."</a><br>".$value['puesto'];
            $col->id_colaborador = "<a href='".route('empleados.show',$value['id_colaborador'])."'>".$value['id_colaborador']."</a>";
            $col->correo_trabajo = "<a href='".$a."'>".$value['correo_trabajo']."</a>";
            $col->ingreso_fecha = "<a href='".$a."'>".$value['ingreso_fecha']."</a>";
            $col->area = "<a href='".$a."'>".$value['area']."</a>";
            $col->puesto = "<a href='".$a."'>".$value['puesto']."</a>";
            $col->supervisor = "<a href='".$a."'>".$value['supervisor']."</a>";
            $col->telefono_movil = "<a href='".$a."'>".$value['telefono_movil']."</a>";
            
            $col->un_imagen = "<a href='".$a."'><img src='".$value['un_imagen']."' style='width:50px; height:20px;'></a>";*/


            if($value['usuario_cognito']!=null){
                $file = "dist/img/colabora/".$value['usuario_cognito'].".png";
                $file2 = "dist/img/colabora/".$value['usuario_cognito'].".jpg";
                $exists = file_exists( $file );
                $exists2 = file_exists( $file2 );
                if($exists==true){
                    $im = $value['usuario_cognito'].'.png';
                }
                if($exists2==true){
                    $im = $value['usuario_cognito'].'.jpg';
                }
                if($exists2 == false && $exists== false){
                    $im = 'sinfoto.png';
                }
            }else{
                 $im = 'sinfoto.png';
            }
            $colaboradores[$key]['nombre'] = "<div><div style='float: left;'><img src='dist/img/colabora/".$im."' style='border-radius: 50%; width: 40px;'></div><div style='float: left;margin-left:4px;'><a href='".route('empleados.show',$value['id_colaborador'])."'>".$value['nombre']."</a><br>".$value['puesto'].'</div></div>';
            $colaboradores[$key]['id_colaborador'] = "<a href='".route('empleados.show',$value['id_colaborador'])."'>".$value['id_colaborador']."</a>";
            $colaboradores[$key]['correo_trabajo'] = $value['correo_trabajo'];
            $colaboradores[$key]['ingreso_fecha'] = date_format(date_create($value['ingreso_fecha']), 'Y-m-d');
            $colaboradores[$key]['area'] = $value['area'];
            $colaboradores[$key]['puesto'] = $value['puesto'];
            $colaboradores[$key]['supervisor'] = $value['supervisor'];
            $colaboradores[$key]['telefono_movil'] = $value['telefono_movil'];
            
            $colaboradores[$key]['un_imagen'] = "<img src='".$value['un_imagen']."' style='width:50px; height:20px;' alt='".$value['un_codigo']."'>";

        }
       
        return DataTables::of($colaboradores)->addColumn('acciones','rh.acciones')->rawColumns(['acciones','nombre','id_colaborador','correo_trabajo','ingreso_fecha','area','puesto','supervisor','telefono_movil','un_imagen'])->make(true);
    }
    public function getMetricas(Request $request)
    {   
        //print_r($request);
        if ($request->isMethod('post')){    
            $metricas = Metricas::where("id_colaborador","=",$request->idcolabora)->orderBy('id_matriz', 'DESC')->get()->toArray();
            foreach ($metricas as $key => $value) {
                if($value['resultado']>=85){
                    $calificacion = '<h2 class="text-success">'.$value['resultado'].'%</h2>';
                }elseif($value['resultado'] < 85 && $value['resultado'] > 69){
                    $calificacion = '<h2 class="text-warning">'.$value['resultado'].'%</h2>';
                }else{
                    $calificacion = '<h2 class="text-danger">'.$value['resultado'].'%</h2>';
                }
                if($value['cerrada'] == 1){
                    $estatus = '<i class="fa fa-fw fa-file"></i>';
                }else{
                    $estatus= '';
                }
                $metricas[$key]['nombre'] = '<p class="text-primary" onclick="seeMetric('.$value['id_matriz'].');">'.$value['nombre'].'</p>';
                $metricas[$key]['resultado'] = '<div align="center">'.$calificacion.'</div>';
                $metricas[$key]['cerrada'] = '<div align="center">'.$estatus.'</div>';
                $metricas[$key]['fecha_ini'] = date_format(date_create($value['fecha_ini']), 'Y-m-d');
                $metricas[$key]['fecha_fin'] = date_format(date_create($value['fecha_fin']), 'Y-m-d');

            }




            $returnHTML = view('rh.listam')->with('metricas', $metricas)->render();
           // $returnHTML = '<h1>perrrooooooooooooooooooooooooooooooooooooooooo</h1>';
            return response()->json(array('success' => true, 'html'=>$returnHTML));
            //return response()->json(['response' => $request->idcolabora]); 
        }

        //return response()->json(['response' => 'This is get method']);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
