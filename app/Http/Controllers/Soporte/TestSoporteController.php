<?php

namespace App\Http\Controllers\Soporte;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\Resource;
use Yajra\DataTables\DataTables;
use App\Http\Controllers\Controller;
use App\ModelosSoporte\SummaryTicket;


class TestSoporteController extends Controller
{
    public function index()
    {
     
        return view('Soporte.CCDashBoard');
    }

    public function tickets()
    {    
        //echo "si llega";
        return view('Soporte.tickets');
        
    }

    

    public function getTickets(){
       
        $ticket = SummaryTicket::where("prioridad","=",1)->get()->toArray();
      
        foreach ($ticket as $key => $value) {
            
            $ticket[$key]['id_caso'] = $value['id_caso'];
            $ticket[$key]['clave_cuenta'] = $value['clave_cuenta'];
            $ticket[$key]['titulo'] = $value['titulo'];
            $ticket[$key]['descripcion'] = $value['descripcion'];
            $ticket[$key]['fecha_creacion'] = date_format(date_create($value['fecha_creacion']), 'Y-m-d');
            $ticket[$key]['id_tipo'] = $value['id_tipo'];
            $ticket[$key]['prioridad'] = $value['prioridad'];
            $ticket[$key]['asignado_a'] = $value['asignado_a'];
        }

            return DataTables::of($ticket)->make(true);

        }
       
       
    
    }
    