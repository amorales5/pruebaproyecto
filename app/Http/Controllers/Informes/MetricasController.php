<?php

namespace App\Http\Controllers\Informes;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Yajra\DataTables\DataTables;

use App\Empleado;
use App\User;
use App\Unidad;
use App\Metricas;
use App\Area;
use App\MetricasColaborador;

class MetricasController extends Controller
{
    public function index()
    {
        $filtros['unidades'] = Unidad::where('codigo_tipounidad','<>','UNS')->get()->toArray();
        $filtros['areas'] = Area::where('activa','=','True')->get()->toArray();
        //$unidades = Unidad::all(); User::where("estado","=",1)->get()->toJson();

        return view('rh.metricascolabora')->with('filtros', $filtros);
       // exit();
         return view('rh.metricascolabora');
    }
    public function informesIndex(){
    	return view('rh.informes.informesdash');
    }
     public function metricasFiltro(Request $request){
        //var_dump($request->un);
       // echo $request->un;
        //exit();
        $wheresClaus = [];
        $unidades = $request->un;
        //print_r($unidades);
       if($unidades!='Todos'){
            array_push($wheresClaus,['codigo_unidad','=',$unidades]);
        } 
        if($request->area!=0){
            array_push($wheresClaus,['id_area','=',$request->area]);
        }
         if($request->estatus!=3){
             array_push($wheresClaus,['cerrada','=',$request->estatus]);
        }
        //if($request->anio!=3){
             array_push($wheresClaus,['anio','=',$request->anio]);
        //}
       // if($request->mes!=3){
             array_push($wheresClaus,['mes','=',$request->mes]);
        //}
        if($request->ingreso!=0){
                switch ($request->ingreso) {
                    case '1':
                        $dias = 11;
                        array_push($wheresClaus,['resultado','>=',85]);
                        break;
                     case '2':
                        $entre = 11;
                        array_push($wheresClaus,['resultado','<',85]);
                        array_push($wheresClaus,['resultado','>',69]);
                        break;   
                    case '3':
                        $dias = 11;
                        array_push($wheresClaus,['resultado','<',69]);
                        break;                    
                    default:
                        # code...
                        break;
                }

     
            //array_push($wheresClaus,['mesesingreso','<',$dias]);
        }

        $colaboradores = MetricasColaborador::where($wheresClaus)->orderBy('id_matriz', 'desc')->get()->toArray();
      	//$colaboradores = MetricasColaborador::all();
      	//var_dump($colaboradores);
      	//exit();
        $colaboradores2 = array();
        //echo json_encode($colaboradores);
        //exit();

        foreach ($colaboradores as $key => $value) {
            //echo $value.'<br>';
            $a = route('empleados.show',$value['id_colaborador']);

            if($value['resultado']>=85){
                $calificacion = '<h3 class="text-success">'.$value['resultado'].'%</h3>';
            }elseif($value['resultado'] < 85 && $value['resultado'] > 69){
                $calificacion = '<h3 class="text-warning">'.$value['resultado'].'%</h3>';
            }else{
                $calificacion = '<h3 class="text-danger">'.$value['resultado'].'%</h3>';
            }
            if($value['cerrada'] == 1){
                $estatus = '<div align="center"><i class="fa fa-fw fa-file"></i></div>';
            }else{
                $estatus= '<div align="center"><i class="fa fa-fw fa-clock-o"></i></div>';
            }
            //$link = "<a href='".route('/metricasone',$value['id_matriz'])."'>".$value['nombre']."</a>";
            $link = "<a href='https://sii.mbgeprojects.net:8091/metricasone/".$value['id_matriz']."'>".$value['nombre']."</a>";
            $colaboradores[$key]['id_matriz'] = $value['id_matriz'];
            $colaboradores[$key]['id_colaborador'] = "<a href='".route('empleados.show',$value['id_colaborador'])."'>".$value['id_colaborador']."</a>";
            $colaboradores[$key]['nombre_colaborador'] = $value['nombre_colaborador'];
            $colaboradores[$key]['fecha_ini'] = date_format(date_create($value['fecha_ini']), 'Y-m-d');
            $colaboradores[$key]['fecha_fin'] = date_format(date_create($value['fecha_fin']), 'Y-m-d');
            $colaboradores[$key]['area'] = $value['area'];
            $colaboradores[$key]['puesto'] = $value['puesto'];
            $colaboradores[$key]['nombre'] = $link;
            $colaboradores[$key]['resultado'] = '<div align="center">'.$calificacion.'</div>';
            $colaboradores[$key]['cerrada'] = $estatus;
            $colaboradores[$key]['url_img'] = "<div align='center'><img src='dist/img/unidades/".$value['url_img']."' style='align:center;width:50px; height:20px;'></div>";
            

        }
       
        return DataTables::of($colaboradores)->rawColumns(['id_matriz','cerrada','nombre','fecha_ini','fecha_fin','resultado','nombre_colaborador','area','url_img'])->make(true);
    }
    public function show($id){
    	return view('rh.informes.informemetrica',array('id_matriz' => $id));
    }
    public function nuevaMetrica(Request $request){
            
            $TipoProceso = 'AddMetric';
            $Tareas = '';

                $UserCognito = 'omar.vazquez23';

            //if ($Cuenta == ''){
                $Cuenta = 'NA';
           // }
            
            // Credenciales Appian
            $username = 'juan.alegria';
            $password = 'Intersistemas1.';
            
                
            // --------------------------------------------------------------------------------------
            // Invocación del proceso
            // --------------------------------------------------------------------------------------
            
            $url = 'https://mbgedev.appiancloud.com/suite/webapi/EjecutaProceso?Tipo=' . $TipoProceso . '&Usuario='. $UserCognito . '&account=' . $request->idcolabora;
                    
            //open connection
            $ch = curl_init($url);

            //set the url, number of POST vars, POST data
            curl_setopt($ch, CURLOPT_URL, $url);
            
            curl_setopt($ch, CURLOPT_USERPWD, "$username:$password");
            curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
            curl_setopt($ch, CURLOPT_HEADER, 0);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            
            //execute post
            $idProceso = curl_exec($ch);
            //echo 'Proceso: ' . $idProceso;

            //close connection
            curl_close($ch);

            $Tareas = '';
            
            // Credenciales Appian
            $username = 'juan.alegria';
            $password = 'Intersistemas1.';
                
            
            // --------------------------------------------------------------------------------------
            // Invocación de reporte de tareas
            // --------------------------------------------------------------------------------------
            
            $url = 'https://mbgedev.appiancloud.com/suite/webapi/tareasproceso?ProcessID=' . $idProceso;
            
            
            //open connection
            $ch = curl_init($url);

            //set the url, number of POST vars, POST data                   
            curl_setopt($ch, CURLOPT_URL, $url);
            
            curl_setopt($ch, CURLOPT_USERPWD, "$username:$password");
            curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
            curl_setopt($ch, CURLOPT_HEADER, 0);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            
            //execute post
            $Tareas = curl_exec($ch);
                                                                    
            //echo 'Tareas: ' . "'". substr($Tareas, 1, strlen($Tareas) - 2) . "';";
           $arrr = explode('c8":',substr($Tareas, 1, strlen($Tareas) - 2));
           $tareaaa = explode(',"c9"',$arrr[1]);
            //echo $tareaaa[0];
            //exit();
            //echo $Tarea->c8;
            
            //close connection
            curl_close($ch); 

            //$returnHTML = '<appian-task taskId="'.$tareaaa.'"></appian-task>';
            return response()->json(array('success' => true, 'html'=>$tareaaa[0]));


    }
}
