<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;

use App\VColaborador;

class EmpleadosController extends Controller
{
     public function show($id)
    {
        //echo $id;

       return view('rh.appiandash',array('id_colaborador' => $id));
       
    }
   	public function metricaempleado($id){
    	return view('rh.informes.informemetrica',array('id_matriz' => $id));
    }
}
