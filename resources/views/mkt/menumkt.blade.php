                
                @section('menus')
                <ul id="js-nav-menu" class="nav-menu">
                   <li>
                      <a href="{{ action('ColaboradoresController@index', ['id' => 1]) }}" title="Administracion y Finanzas" data-filter-tags="application intel">
                          <i class="fa fa-info-circle" aria-hidden="true"></i>
                          <span class="nav-link-text" data-i18n="nav.application_intel">Dashboard</span>
                      </a>
                    </li>
                    <li>
                      <a href="#" title="Recursos Humanos" data-filter-tags="application intel">
                          <i class="fa fa-cogs" aria-hidden="true"></i>
                          <span class="nav-link-text" data-i18n="nav.application_intel">Plan de Marketing</span>
                      </a>
                    </li>
                    <li>
                      <a href="#" title="Servicio al Cliente" data-filter-tags="application intel">
                          <i class="fa fa-tag" aria-hidden="true"></i>
                          <span class="nav-link-text" data-i18n="nav.application_intel">Lead Management</span>
                      </a>
                    </li>
                    <li>
                      <a href="#" title="Proyectos" data-filter-tags="application intel">
                          <i class="fa fa-calendar" aria-hidden="true"></i>
                          <span class="nav-link-text" data-i18n="nav.application_intel">Calendario de estrategias</span>
                      </a>
                    </li>
                    <li>
                      <a href="#" title="Ente Estrategico" data-filter-tags="application intel">
                          <i class="fa fa-thumbs-up" aria-hidden="true"></i>
                          <span class="nav-link-text" data-i18n="nav.application_intel">Monitor de Campaña</span>
                      </a>
                    </li>
                    <li>
                      <a href="#" title="Ente Estrategico" data-filter-tags="application intel">
                          <i class="fa fa-handshake-o" aria-hidden="true"></i>
                          <span class="nav-link-text" data-i18n="nav.application_intel">Cominicacion e identidad Corporativa</span>
                      </a>
                    </li>
                    <li>
                      <a href="#" title="Ente Estrategico" data-filter-tags="application intel">
                          <i class="fa fa-balance-scale" aria-hidden="true"></i>
                          <span class="nav-link-text" data-i18n="nav.application_intel">Presupuesto</span>
                      </a>
                    </li>
                                                                             
                </ul>
                @endsection