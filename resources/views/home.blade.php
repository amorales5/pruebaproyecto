@extends('layouts.admin')

@section('content')
<div class="subheader" align="center">
 <!-- <h1 class="subheader-title">SII Sistema Intelignete</h1>-->
  <div class="row">
    <div class="col-lg-12" align="center">
      <img src="{{ asset('dist/img/siiimages/SII-02.png') }}" alt="" style="width:15%;">
    </div>
  </div>
</div>
<div class="row">
  <div class="col-lg-3">
    <div class="card text-center bg-success efecto">
      <a href="https://sii.mbgeprojects.net:8089/dashboard.php" style="text-decoration:none;color:black;">
        <div class="card-body">
            <h5 class="card-title"><i class="fas fa-balance-scale fa-3x"></i></h5>
            <h3><strong>Administración y Finanzas</strong></h3>
            <p class="card-text" style="color:gray !important;">Desiciones Financieras Inteligentes <br>&nbsp;</p>
        </div>
      </a>
    </div>
  </div>
  <div class="col-lg-3">
    <div class="card text-center bg-danger-50 efecto">
      <a href="{{ action('RedirectController@index', ['id' => 1]) }}" style="text-decoration:none;color:black;">
        <div class="card-body">
            <h5 class="card-title"><i class="fas fa-heartbeat fa-3x"></i></h5>
            <h3><strong>Recursos Humanos</strong></h3>
            <p class="card-text" style="color:gray !important;">Cultura, Plan de Crecimineto y Bienestar <br>&nbsp;</p>
        </div>
      </a>
    </div>
  </div>
  <div class="col-lg-3">
    <div class="card text-center bg-warning efecto">
    <a href="{{ action('RedirectController@index', ['id' => 2]) }}" style="text-decoration:none;color:black;">
        <div class="card-body">
            <h5 class="card-title"><i class="fas fa-phone fa-3x" style="color:black;"></i></h5>
            <h3 style="color:black;"><strong>Servicio al Cliente</strong></h3>
            <p class="card-text" style="color:gray !important;">Garantizamos un experiencia maravillosa al Cliente<br>&nbsp;</p>
        </div>
        </a>
    </div>
  </div>
  <div class="col-lg-3">
    <div class="card text-center bg-info-400 efecto" >
        <div class="card-body">
            <h5 class="card-title"><i class="fas fa-tasks fa-3x" style="color:black;"></i></h5>
            <h3 style="color:black;"><strong>Proyectos</strong></h3>
            <p class="card-text" style="color:gray !important;">Metodologia y Entrega con alto Valor <br>&nbsp;</p>
        </div>
    </div>
  </div>
</div>
<br>
<div class="row">
  <div class="col-lg-3">
    <div class="card text-center bg-primary-400 efecto">
        <div class="card-body">
            <h5 class="card-title"><i class="far fa-lightbulb fa-3x"></i></h5>
            <h3 color="black"><strong>Ente Estratégico</strong></h3>
            <p class="card-text" style="color:lightgray !important;">Espacio Creativo Exponenciando <br> &nbsp;</p>
        </div>
    </div>
  </div>
  <div class="col-lg-3">
    <div class="card text-center bg-danger-600 efecto">
        <div class="card-body">
            <h5 class="card-title"><i class="fas fa-filter fa-3x"></i></h5>
            <h3><strong>Marketing</strong></h3>
            <p class="card-text" style="color:lightgray !important;">Ampliar oportunidades de Negocio y Posicioamiento de marca </p>
        </div>
    </div>
  </div>
  <div class="col-lg-3">
    <div class="card text-center bg-fusion-400 efecto">
        <div class="card-body">
            <h5 class="card-title"><i class="fas fa-globe fa-3x"></i></h5>
            <h3><strong>Comunidad</strong></h3>
            <p class="card-text" style="color:lightgray !important;">Redes de Valor y Colaboración (Colaboradores,
            clientes, Proveedores y Alianzas)</p>
        </div>
    </div>
  </div>
  <div class="col-lg-3">
    <div class="card text-center bg-gray-300 efecto">
        <div class="card-body">
            <h5 class="card-title"><i class="fas fa-graduation-cap fa-3x"></i></h5>
            <h3><strong>LMS (Academia)</strong></h3>
            <p class="card-text" style="color:gray !important;">Sistema de Administacion del Conocimiento <br>&nbsp;</p>
        </div>
    </div>
  </div>
</div>

@endsection
