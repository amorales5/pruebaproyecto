@extends('layouts.admin')
@extends('menu')
@section('content')
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row">
          <div class="col-lg-12 col-sm-12" align="center">
            <h1>SII Sistema Intelissssgente</h1>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

            


    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <!-- Small boxes (Stat box) -->
 
        <div class="row">
          <div class="col-lg-1"></div>
          <div class="col-lg-10">
            <div class="row">
                <div class="col-sm-4">
                  <div class="card border-default mb-3" id="adminfin">
                    <div class="card-body text-default" id="adminfinBody">
                      <div class="row">
                        <div class="col-sm-12" align="right">
                          <span data-toggle="popover" title="Administracion y finanzas" data-content="Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                          tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                          quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                          consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
                          cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
                          proident, sunt in culpa qui officia deserunt mollit anim id est laborum."><i class="fa fa-info-circle" aria-hidden="true"></i></span>
                        </div>
                      </div>
                      <div align="center"><span><i class="fa fa-balance-scale fa-2x" aria-hidden="true"></i></span></div>
                      <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                    </div>
                    <div class="card-footer border-default" style="background-color: #28a745;">Administracion y finanzaz</div>
                  </div>
                </div>
                <div class="col-sm-4">
                  <div class="card border-default mb-3" id="rh">
                    <div class="card-body text-default" id="rhBody">
                      <div class="row">
                        <div class="col-sm-12" align="right">
                          <span data-toggle="popover" title="Recursos humanos" data-content="Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                          tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                          quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                          consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
                          cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
                          proident, sunt in culpa qui officia deserunt mollit anim id est laborum."><i class="fa fa-info-circle" aria-hidden="true"></i></span>
                        </div>
                      </div>
                      <div align="center"><span><i class="fa fa-heartbeat fa-2x" aria-hidden="true"></i></span></div>
                      <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                    </div>
                    <div class="card-footer border-default" style="background-color: #00c0ef;">Recursos humanos</div>
                  </div>
                </div>
                <div class="col-sm-4">
                  <div class="card border-default mb-3 ov" id="customerService">
                    <div class="card-body text-default" id="customerServiceBody">
                      <div class="row">
                        <div class="col-sm-12" align="right">
                          <span data-toggle="popover" title="Servicio al cliente" data-content="Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                          tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                          quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                          consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
                          cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
                          proident, sunt in culpa qui officia deserunt mollit anim id est laborum."><i class="fa fa-info-circle" aria-hidden="true"></i></span>
                        </div>
                      </div>
                      <div align="center"><span><i class="fa fa-tty fa-2x" aria-hidden="true"></i></span></div>
                      <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                    </div>
                    <div class="card-footer border-default" style="background-color: #39CCCC;">Servicio al cliente</div>
                  </div>
                </div>
            </div>
          </div>
          <div class="col-lg-1"></div>
        </div>
        <div class="row">
          <div class="col-lg-1"></div>
          <div class="col-lg-10">
            <div class="row">
                <div class="col-sm-4">
                  <div class="card border-default mb-3 ov" id="proyects">
                    <div class="card-body text-default" id="proyectsBody">
                      <div class="row">
                        <div class="col-sm-12" align="right">
                          <span  data-toggle="popover" title="Proyectos" data-content="Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                          tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                          quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                          consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
                          cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
                          proident, sunt in culpa qui officia deserunt mollit anim id est laborum."><i class="fa fa-info-circle" aria-hidden="true"></i></span>
                        </div>
                      </div>
                      <div align="center"><span><i class="fa fa-tasks fa-2x" aria-hidden="true"></i></span></div>
                      <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                    </div>
                    <div class="card-footer border-default" style="background-color: #605ca8;">Proyectos</div>
                  </div>
                </div>
                <div class="col-sm-4">
                  <div class="card border-default mb-3 ov" id="comunity">
                    <div class="card-body text-default" id="comunityBody">
                      <div class="row">
                        <div class="col-sm-12" align="right">
                          <span data-toggle="popover" title="Comunidad" data-content="Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                          tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                          quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                          consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
                          cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
                          proident, sunt in culpa qui officia deserunt mollit anim id est laborum."><i class="fa fa-info-circle" aria-hidden="true"></i></span>
                        </div>
                      </div>
                      <div align="center"><span><i class="fa fa-users fa-2x" aria-hidden="true"></i></span></div>
                      <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                    </div>
                    <div class="card-footer border-default" style="background-color: #f39c12;">Comunidad</div>
                  </div>
                </div>
                <div class="col-sm-4">
                  <div class="card border-default mb-3 ov" id="lms">
                    <div class="card-body text-default" id="lmsBody">
                      <div class="row">
                        <div class="col-sm-12" align="right">
                          <span data-toggle="popover" title="LMS" data-content="Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                          tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                          quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                          consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
                          cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
                          proident, sunt in culpa qui officia deserunt mollit anim id est laborum."><i class="fa fa-info-circle" aria-hidden="true"></i></span>
                        </div>
                      </div>
                      <div align="center"><span><i class="fa fa-graduation-cap fa-2x" aria-hidden="true"></i></span></div>
                      <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                    </div>
                    <div class="card-footer border-default" style="background-color: #3c8dbc;">LMS</div>
                  </div>
                </div>
            </div>
          </div>
          <div class="col-lg-1"></div>
        </div>
        <!-- /.row (main row) -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
@endsection
