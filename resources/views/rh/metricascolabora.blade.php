@extends('layouts.admin')
@extends('rh.menu')
@section('content')
    <!-- Content Header (Page header) -->
 <!--   <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Colaboradores</h1>
          </div>

          <div class="col-sm-6">
          </div>
        </div>
        <!-- /.row -->
    <!--  </div><!-- /.container-fluid -->
   <!-- </div> -->
    <!-- /.content-header -->

            


    <!-- Main content -->
   <!-- <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-tittle">ddd</h3>
            </div>
            <div class="box-body">
              <div class="col-xs-6"></div>
            </div>
          </div>
        </div>
      </div>
    </section>-->
    <style>
        ul.avatars {
            display: flex ; /* Causes LI items to display in row. */
            list-style-type: none ;
            margin: auto ; /* Centers vertically / horizontally in flex container. */
            padding: 0px 7px 0px 0px ;
            z-index: 1 ; /* Sets up new stack-container. */
        }
        li.avatars__item {
            height: 49px ;
            margin: 0px 0px 0px 0px ;
            padding: 0px 0px 0px 0px ;
            
            width: 42px ; /* Forces flex items to be smaller than their contents. */
        }
        /*
            These zIndex values will only be relative to the contents of the UL element,
            which sets up its own stack container. As such, these will only be relevant
            to each other, not to the page at large.
            --
            NOTE: We could theoretically get around having to set explicit zIndex values
            by using "flex-direction: row-reverse" and using the natural stacking order
            of the DOM; however, to do that, we'd have to reverse the order of the HTML
            elements, which feels janky and unnatural.
        */
        li.avatars__item:nth-child( 1 ) { z-index: 9 ; }
        li.avatars__item:nth-child( 2 ) { z-index: 8 ; }
        li.avatars__item:nth-child( 3 ) { z-index: 7 ; }
        li.avatars__item:nth-child( 4 ) { z-index: 6 ; }
        li.avatars__item:nth-child( 5 ) { z-index: 5 ; }
        li.avatars__item:nth-child( 6 ) { z-index: 4 ; }
        li.avatars__item:nth-child( 7 ) { z-index: 3 ; }
        li.avatars__item:nth-child( 8 ) { z-index: 2 ; }
        li.avatars__item:nth-child( 9 ) { z-index: 1 ; }
        /*
            These items are all 49px wide while the flex-item (in which they live) is
            only 42px wide. As such, there will be several pixels of overflow content,
            which will be displayed in a reverse-stacking order based on above zIndex.
        */
        img.avatars__img,
        span.avatars__initials,
        span.avatars__others {
            background-color: #596376 ;
           
            border-radius: 100px 100px 100px 100px ;
            color: #FFFFFF ;
            
            font-family: sans-serif ;
            font-size: 12px ;
            font-weight: 100 ;
            height: 45px ;
            line-height: 45px ;
            text-align: center ;
            width: 45px ;
        }
        span.avatars__others {
            background-color: #1E8FE1 ;
        }
    </style>
    <section class="content">
      <div class="container-fluid">
      <div class="row">
        <div class="col-md-12">
          <div id="panel-1" class="panel">
              <div class="panel-hdr">
                  <h2>
                    Metricas

                  </h2>
                  <div class="panel-toolbar">
                     
                  </div>
              </div>  
              <div class="panel-container show">
                <div class="panel-content">
                  <div class="row">
                    <div class="col-md-2">
                      <label>UN</label>
                       <select id="unidad" class="form-control select2" onchange="filterColabora()">
                        <option value="Todos" selected>Todos</option>
                        <option value="STAFF">STAFF</option>
                        @foreach ($filtros['unidades'] as $un)
                            <option value="{{ $un['codigo_unidad'] }}">{{ $un['descripcion'] }}</option>    
                        @endforeach 
                      </select>
                    </div>
                    <div class="col-md-2">
                      <label>Area</label>
                      <select id="area" class="form-control select2" onchange="filterColabora()">
                        <option value="0">Todas</option>
                          @foreach ($filtros['areas'] as $area)
                            <option value="{{ $area['id_area'] }}">{{ $area['nombre'] }}</option>    
                          @endforeach 
                      </select>
                    </div>
                    <div class="col-md-2">
                      <label>Año</label>
                      <select id="anio" class="form-control select2" onchange="filterColabora()">
                        <option value="2019">2019</option>
                        <option value="2018">2018</option>
                        <option value="2017">2017</option>
                        <option value="2016">2016</option>
                      </select>
                    </div>
                    <div class="col-md-2">
                      <label>Mes</label>
                      <select id="mes" class="form-control select2" onchange="filterColabora()">
                        <option value="1">Enero</option>
                        <option value="2">Febrero</option>
                        <option value="3">Marzo</option>
                        <option value="4">Abril</option>
                        <option value="5">Mayo</option>
                        <option value="6">Junio</option>
                        <option value="7">Julio</option>
                        <option value="8">Agosto</option>
                        <option value="9">Septiembre</option>
                        <option value="10">Octubre</option>
                        <option value="11">Noviembre</option>
                        <option value="12">Diciembre</option>
                      </select>
                    </div>

                    <div class="col-md-2">
                      <label>Calificaciones</label>
                      <select id="ingreso" class="form-control select2" onchange="filterColabora()">
                        <option value="0">-Todas-</option>
                        <option value="1">85 a 100</option>
                        <option value="2">70 a 85</option>
                        <option value="3">Menor a 70</option>
                      </select>
                    </div>
                    <div class="col-md-2">
                      <label>Estatus</label>
                      <select id="estatus" class="form-control select2" onchange="filterColabora();">
                        <option value="3">Todos</option>
                        <option value="1" selected>Activa</option>
                        <option value="0">Cerrada</option>
                      </select>
                    </div>




                  </div>                  
                 
                 <br>
                <div class="row">
                  <div class="col-md-12">
                    <table class="table table-bordered table-hover table-striped w-100" id="tableGrid">
                      <thead class="bg-warning-200">
                        <tr>
                          <th>ID</th>
                          <th>Estatus</th>
                          <th>Nombre</th>
                          <th>inicio</th>
                          <th>Fin</th>
                          <th>Score</th>
                          <th>Colaborador</th>
                          <th>Unidad</th>
                          <th>Area</th>
                          
                        </tr>
                      </thead>
                      <tbody>
                       <!-- <tr>
                          <td>1</td>
                          <td>omar</td>
                          <td>ovazquez@gmail.com</td>
                        </tr> -->
                      </tbody>
                    </table>
                  </div>
                </div>
              </div> 
          </div>           


        </div>
      </div>
 





        <!--<div class="row">
          <div class="col-md-12">
            <div class="card card-primary card-outline">
              <div class="card-header">
                <div class="car-title">
                  Metricas
                </div>
              </div>
              <div class="card-body">

                <br>

              </div>
            </div>
          </div>
        </div>-->


    
      </div><!-- /.container-fluid -->
    </section>

    <!-- /.content -->
@endsection

@section('scriptpage')
<!-- DataTable -->
<meta name="csrf-token" content="{{ csrf_token() }}">
<script>
            $.ajaxSetup({
            headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
          });
  $(document).ready(function() {
    filterColabora();
    $('.select2').select2({ width: '100%',height:'100%' });
   /* $('#tableGrid').DataTable({
      processing: true,
      serverSide: true,
      responsive : true,
      ajax: "{{ route('api.getcolaboradores') }}",
      columns: [
        {data: 'id_colaborador', name: 'id_colaborador'},
        {data: 'nombre', name: 'nombre'},
        {data: 'correo_trabajo', name: 'correo_trabajo'},
        {data: 'telefono_movil', name: 'telefono_movil'},
        {data: 'ingreso_fecha', name: 'ingreso_fecha'},
        {data: 'un_imagen', name: 'un_imagen'},
        {data: 'area', name: 'area'},
        
      ],
      language: {
          "decimal": "",
          "emptyTable": "No hay información",
          "info": "Mostrando _START_ a _END_ de _TOTAL_ Entradas",
          "infoEmpty": "Mostrando 0 to 0 of 0 Entradas",
          "infoFiltered": "(Filtrado de _MAX_ total entradas)",
          "infoPostFix": "",
          "thousands": ",",
          "lengthMenu": "Mostrar _MENU_ Entradas",
          "loadingRecords": "Cargando...",
          "processing": "Procesando...",
          "search": "Buscar: ",
          "zeroRecords": "Sin resultados encontrados",
          "paginate": {
              "first": "Primero",
              "last": "Ultimo",
              "next": "Siguiente",
              "previous": "Anterior"
          }
      },
      dom: 'Bfrtip', 
      buttons: [
          {
              extend:    'copyHtml5',
              text:      '<i class="fa fa-copy"></i> Copy',
              titleAttr: 'Copy'
          },
          {
              extend:    'excelHtml5',
              text:      '<i class="fa fa-file-excel-o"></i> Excel',
              titleAttr: 'Excel'
          },
          {
              extend:    'pdfHtml5',
              text:      '<i class="fa fa-file-pdf-o"></i> PDF',
              titleAttr: 'PDF'
          }
      ],
    }); */
  });
  function filterColabora(){
  
    $("#tableGrid").dataTable().fnDestroy();
    $('#tableGrid').DataTable({
      processing: true,
      serverSide: true,
      responsive : true,
      ajax: {
        url : "{{ route('metricasFiltro') }}",
        type : 'POST',
        data : {
            un : $('#unidad').val(),
            area : $('#area').val(),
            ingreso : $('#ingreso').val(),
            estatus : $('#estatus').val(),
            anio : $('#anio').val(),
            mes : $('#mes').val(),
        }
      },
      columns: [
        {data: 'id_matriz', name: 'id_matriz'},
        {data: 'cerrada', name: 'cerrada'},
        {data: 'nombre', name: 'nombre'},
        {data: 'fecha_ini', name: 'fecha_ini'},
        {data: 'fecha_fin', name: 'fecha_fin'},
        {data: 'resultado', name: 'resultado'},
        {data: 'nombre_colaborador', name: 'nombre_colaborador'},
        {data: 'url_img', name: 'url_img'},
        {data: 'area', name: 'area'},
        
      ],
      language: {
          "decimal": "",
          "emptyTable": "No hay información",
          "info": "Mostrando _START_ a _END_ de _TOTAL_ Entradas",
          "infoEmpty": "Mostrando 0 to 0 of 0 Entradas",
          "infoFiltered": "(Filtrado de _MAX_ total entradas)",
          "infoPostFix": "",
          "thousands": ",",
          "lengthMenu": "Mostrar _MENU_ Entradas",
          "loadingRecords": "Cargando...",
          "processing": "Procesando...",
          "zeroRecords": "Sin resultados encontrados",
          "paginate": {
              "first": "Primero",
              "last": "Ultimo",
              "next": "Siguiente",
              "previous": "Anterior"
          }
      },
      dom: "<'row mb-3'<'col-sm-12 col-md-6 d-flex align-items-center justify-content-start'f><'col-sm-12 col-md-6 d-flex align-items-center justify-content-end'B>>" +
                        "<'row'<'col-sm-12'tr>>" +
                        "<'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7'p>>",
      buttons: [
          {
              extend: 'colvis',
              text: 'Columnas',
              titleAttr: 'Visibilidad',
              className: 'btn-outline-default btn-sm mr-1'
          },
          {
              extend: 'pdfHtml5',
              text: 'PDF',
              titleAttr: 'Generate PDF',
              className: 'btn-outline-danger btn-sm mr-1'
          },
          {
              extend: 'excelHtml5',
              text: 'Excel',
              titleAttr: 'Generate Excel',
              className: 'btn-outline-success btn-sm mr-1'
          },
          {
              extend: 'copyHtml5',
              text: 'Copy',
              titleAttr: 'Copy to clipboard',
              className: 'btn-outline-primary btn-sm mr-1'
          },
          {
              extend: 'print',
              text: 'Print',
              titleAttr: 'Print Table',
              className: 'btn-outline-primary btn-sm'
          }

      ],
    }); 
  }
</script>
@endsection