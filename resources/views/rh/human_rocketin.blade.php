@extends('layouts.admin')
@extends('rh.menu')
@section('content')
    <!-- Content Header (Page header) -->

            


    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <!-- Small boxes (Stat box) -->
<div class="row"></div>
        <br><br>
        <div class="row">
          <div class="col-md-1"></div>
          <div class="col-md-2">
            
          </div>
          <div class="col-md-2">
           <!-- <div class="hexagono2" style="float:left;" id="atraccionHex">
              <span class="hexagono-content">
                <i class="fa fa-search fa-2x" aria-hidden="true"></i>
                <br>
                <strong style="font-size: 70%;">Atraccion y Seleccion de talento</strong>
              </span>
            </div>-->
          </div>
          <div class="col-md-2">
            <a href="{{ action('ColaboradoresController@index', ['id' => 1]) }}">
            <div class="hexagono2" style="float:left; background-color: #F64BEE" id="colaboradorHex">
              <span class="hexagono-content">
                <i class="fa fa-users fa-2x" aria-hidden="true"></i>
                <br>
                <strong style="font-size: 70%;">Colaboradores</strong>
              </span>
            </div>
            </a>
          </div>
          <div class="col-md-2">
           <!-- <div class="hexagono2" style="float:left;" id="retencionHex">
              <span class="hexagono-content">
                <i class="fa fa-thumbs-up fa-2x" aria-hidden="true"></i>
                <br>
                <strong style="font-size: 70%;">Retencion de Talento</strong>
              </span>
            </div>-->
          </div>
          <div class="col-md-2">
          </div>
          <div class="col-md-1">
          </div>
          <!--<div class="col-sm-12">
            <div class="hexagono2" style="float:left;"></div>
            <div class="hexagono2" style="float:left;"></div>
            <div class="hexagono2" style="float:left;"></div>
            <div class="hexagono2" style="float:left;"></div>
            <div class="hexagono2" style="float:left;"></div>
          </div> -->
        </div>
        <br><br>
        <div class="row">
        <!--  <div class="col-md-6">
            <div class="row">
              <div class="col-sm-4"></div>
              <div class="col-sm-4">
              <div class="hexagono2" style="float:left;" id="compensacionesHex">
              <span class="hexagono-content">
                <i class="fa fa-user-plus fa-2x" aria-hidden="true"></i>
                <br>
                <strong style="font-size: 70%;">Atraccion Talento</strong>
              </span>
            </div>
              </div>
              <div class="col-sm-4">
                            <div class="hexagono2" style="float:left;" id="compensacionesHex">
              <span class="hexagono-content">
                <i class="fa fa-user-plus fa-2x" aria-hidden="true"></i>
                <br>
                <strong style="font-size: 70%;">Atraccion Talento</strong>
              </span>
            </div>
              </div>
            </div>
          </div>
          <div class="col-md-6">
            <div class="row">
              
              <div class="col-sm-2">
                            <div class="hexagono2" style="float:left;" id="compensacionesHex">
              <span class="hexagono-content">
                <i class="fa fa-user-plus fa-2x" aria-hidden="true"></i>
                <br>
                <strong style="font-size: 70%;">Atraccion Talento</strong>
              </span>
            </div>
              </div>
              <div class="col-sm-2">
                            <div class="hexagono2" style="float:left;" id="compensacionesHex">
              <span class="hexagono-content">
                <i class="fa fa-user-plus fa-2x" aria-hidden="true"></i>
                <br>
                <strong style="font-size: 70%;">Atraccion Talento</strong>
              </span>
            </div>
              </div>
              <div class="col-sm-8"></div>
            </div>
          </div> -->



           
          <div class="col-md-2"></div>
          <div class="col-md-2">
            <div class="hexagono2" style="float:left;margin-left: 30%;" id="compensacionesHex">
              <span class="hexagono-content">
                <i class="fa fa-user-plus fa-2x" aria-hidden="true"></i>
                <br>
                <strong style="font-size: 70%;">Atraccion Talento</strong>
              </span>
            </div>
          </div>
          <div class="col-md-2">
            <div class="hexagono2" style="float:left;" id="desarrolloHex">
              <span class="hexagono-content">
                <i class="fa fa-line-chart fa-2x" aria-hidden="true"></i>
                <br>
                <strong style="font-size: 70%;">Desarrollo Organizacional</strong>
              </span>
            </div>
          </div>
          <div class="col-md-1">
            <div class="hexagono2" style="float:left;" id="saludHex">
              <span class="hexagono-content">
                <i class="fa fa-heartbeat fa-2x" aria-hidden="true"></i>
                <br>
                <strong style="font-size: 70%;">Salud y Seguridad</strong>
              </span>
            </div>
          </div>
          <div class="col-md-2">
      
            <div class="hexagono2" style="float:left;margin-left:25%;" id="relacionesHex">
              <span class="hexagono-content">
                <i class="fa fa-thumbs-up fa-2x" aria-hidden="true"></i>
                <br>
                <strong style="font-size: 70%;">Retencion Talento</strong>
              </span>
            </div>
          
          </div>

          <div class="col-md-2"></div>
          <!--<div class="col-sm-12">
            <div class="hexagono2" style="float:left;"></div>
            <div class="hexagono2" style="float:left;"></div>
            <div class="hexagono2" style="float:left;"></div>
            <div class="hexagono2" style="float:left;"></div>
            <div class="hexagono2" style="float:left;"></div>
          </div> -->
        </div>
        <br><br>
        <div class="row">
          <div class="col-md-5"></div>
          <div class="col-md-2">
            <div class="hexagono2" style="float:left;" id="relacionesHex">
              <span class="hexagono-content">
                <i class="fa fa-handshake-o fa-2x" aria-hidden="true"></i>
                <br>
                <strong style="font-size: 70%;">Relaciones Laborales</strong>
              </span>
            </div>
          </div>
          <div class="col-md-5"></div>
        </div>
        <br><br>
        <div class="row">
          <div class="col-md-4"></div>
          <div class="col-md 2">
            <div class="hexagono2" style="float:left;" id="saludHex">
              <span class="hexagono-content">
                <i class="fa fa-balance-scale fa-2x" aria-hidden="true"></i>
                <br>
                <strong style="font-size: 70%;">Compensaciones Beneficios</strong>
              </span>
            </div>
          </div>
          <div class="col-md 2">
            <a href="{{ action('Informes\MetricasController@informesIndex') }}">
            <div class="hexagono2" style="float:left;background-color: #23A471" id="saludHex">
              <span class="hexagono-content">
                <i class="fa fa-bar-chart fa-2x" aria-hidden="true"></i>
                <br>
                <strong style="font-size: 70%;">Informes</strong>
              </span>
            </div>
            </a>
          </div>
          <div class="col-md-4"></div>
        </div>
        <br>
        <br>
      
        <div class="row">
          <div class="col-md-3"></div>
          <div class="col-md-2">
            <div class="hexagono2" style="float:left;" id="saludHex">
              <span class="hexagono-content">
                <i class="fa fa-book fa-2x" aria-hidden="true"></i>
                <br>
                <strong style="font-size: 70%;">Documentacion</strong>
              </span>
            </div>
          </div>
          <div class="col-md-2"></div>
          <div class="col-md-2">
            <div class="hexagono2" style="float:left;background-color: #4184FA" id="saludHex">
              <span class="hexagono-content">
                <i class="fa fa-gears fa-2x" aria-hidden="true"></i>
                <br>
                <strong style="font-size: 70%;">Sistema</strong>
              </span>
            </div>
          </div>
          <div class="col-md-3"></div>
        </div>
     <!-- <div class="row">
          <div class="col-lg-3 col-6">
            <div class="small-box bg-info">
              <div class="inner">
                <h3>150</h3>
                <p>New Orders</p>
              </div>
              <div class="icon">
                <i class="ion ion-bag"></i>
              </div>
              <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
            </div>
          </div>
          <div class="col-lg-3 col-6">
            <div class="small-box bg-success">
              <div class="inner">
                <h3>53<sup style="font-size: 20px">%</sup></h3>
                <p>Bounce Rate</p>
              </div>
              <div class="icon">
                <i class="ion ion-stats-bars"></i>
              </div>
              <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
            </div>
          </div>
          <div class="col-lg-3 col-6">
            <div class="small-box bg-warning">
              <div class="inner">
                <h3>44</h3>
                <p>User Registrations</p>
              </div>
              <div class="icon">
                <i class="ion ion-person-add"></i>
              </div>
              <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
            </div>
          </div>
          <div class="col-lg-3 col-6">
            <div class="small-box bg-danger">
              <div class="inner">
                <h3>65</h3>
                <p>Unique Visitors</p>
              </div>
              <div class="icon">
                <i class="ion ion-pie-graph"></i>
              </div>
              <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
            </div>
          </div>
        </div> -->
        <!-- /.row (main row) -->

      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
@endsection
