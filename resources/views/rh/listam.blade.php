                <div class="row">
                  <div class="col-md-12">
                   <!-- <table class="table-bordered table-hover" id="tableMetrics" style="width: 100%;">
                      <thead>
                        <tr>
                          <th>Estatus</th>
                          <th>Nombre</th>
                          <th>Inicio</th>
                          <th>Final</th>
                          <th>Resultado</th>
                          <th>Observacsssiones</th>
                        </tr>
                      </thead>
                      <tbody>
                      @foreach ($metricas as $un)
                          <tr>
                            <td>{!! $un['cerrada'] !!}</td>
                            <td>{!! $un['nombre'] !!}</td>
                            <td>{{ $un['fecha_ini'] }}</td>
                            <td>{{ $un['fecha_fin'] }}</td>
                            <td>{!! $un['resultado'] !!}</td>
							              <td>{{ $un['observaciones'] }}</td>
                          </tr>   
                      @endforeach
                      </tbody>
                    </table>-->
                    <table class="table table-bordered table-hover table-striped w-100" id="tableMetrics">
                      <thead class="bg-warning-200">
                        <tr>
                          <th>Estatus</th>
                          <th>Nombre</th>
                          <th>Inicio</th>
                          <th>Final</th>
                          <th>Resultado</th>
                          <th>Observacsssiones</th>
                        </tr>
                      </thead>
                      <tbody>
                      @foreach ($metricas as $un)
                          <tr>
                            <td>{!! $un['cerrada'] !!}</td>
                            <td>{!! $un['nombre'] !!}</td>
                            <td>{{ $un['fecha_ini'] }}</td>
                            <td>{{ $un['fecha_fin'] }}</td>
                            <td>{!! $un['resultado'] !!}</td>
                            <td>{{ $un['observaciones'] }}</td>
                          </tr>   
                      @endforeach
                      </tbody>
                    </table>
                  </div>
                </div>
<script>
  $(document).ready(function() {
 
  $('#tableMetrics').DataTable({
      language: {
          "decimal": "",
          "emptyTable": "No hay información",
          "info": "Mostrando _START_ a _END_ de _TOTAL_ Entradas",
          "infoEmpty": "Mostrando 0 to 0 of 0 Entradas",
          "infoFiltered": "(Filtrado de _MAX_ total entradas)",
          "infoPostFix": "",
          "thousands": ",",
          "lengthMenu": "Mostrar _MENU_ Entradas",
          "loadingRecords": "Cargando...",
          "processing": "Procesando...",
          
          "zeroRecords": "Sin resultados encontrados",
          "paginate": {
              "first": "Primero",
              "last": "Ultimo",
              "next": "Siguiente",
              "previous": "Anterior"
          }
      },
      responsive : true,
      dom: "<'row mb-3'<'col-sm-12 col-md-6 d-flex align-items-center justify-content-start'f><'col-sm-12 col-md-6 d-flex align-items-center justify-content-end'B>>" +
                        "<'row'<'col-sm-12'tr>>" +
                        "<'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7'p>>", 
      buttons: [
          {
              extend: 'colvis',
              text: '<i class="fas fa-columns"></i>&nbsp;Columnas',
              titleAttr: 'Mostrar/Ocultar Columnas',
              className: 'btn-outline-primary btn-sm mr-1'
          },
          {
              extend: 'pdfHtml5',
              text: '<i class="fas fa-file-pdf"></i>&nbsp; PDF',
              titleAttr: 'Generar PDF',
              className: 'btn-outline-danger btn-sm mr-1'
          },
          {
              extend: 'excelHtml5',
              text: '<i class="fas fa-file-excel"></i>&nbsp; Excel',
              titleAttr: 'Generar Excel',
              className: 'btn-outline-success btn-sm mr-1'
          },
          {
              extend: 'copyHtml5',
              text: '<i class="fas fa-copy"></i> &nbsp;Copiar',
              titleAttr: 'Copiar Tabla',
              className: 'btn-outline-primary btn-sm mr-1'
          },
          {
              extend: 'print',
              text: '<i class="fas fa-print"></i>&nbsp; Imprimir',
              titleAttr: 'Imprimir Tabla',
              className: 'btn-outline-primary btn-sm'
          },
          {
              text: '<a href="javascript:void(0);" class="btn btn-outline-primary btn-lg btn-icon rounded-circle hover-effect-dot waves-effect waves-themed" onclick="newMetric()"><i class="fa fa-plus" aria-hidden="true"></i></a>',
              titleAttr: 'Alta Metrica',
          }

      ],
    }); 
$("#metricsDiv").load("#appianContentDiv");

  });

</script>