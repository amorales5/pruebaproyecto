@extends('layouts.admin')
@extends('rh.menu')
@section('content')
    <!-- Content Header (Page header) -->
 <!--   <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Colaboradores</h1>
          </div>

          <div class="col-sm-6">
          </div>
        </div>-->
        <!-- /.row -->
    <!--  </div>--><!-- /.container-fluid -->
   <!-- </div> -->
    <!-- /.content-header -->

            


    <!-- Main content -->
   <!-- <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-tittle">ddd</h3>
            </div>
            <div class="box-body">
              <div class="col-xs-6"></div>
            </div>
          </div>
        </div>
      </div>
    </section>-->
    <style>
        ul.avatars {
            display: flex ; /* Causes LI items to display in row. */
            list-style-type: none ;
            margin: auto ; /* Centers vertically / horizontally in flex container. */
            padding: 0px 7px 0px 0px ;
            z-index: 1 ; /* Sets up new stack-container. */
        }
        li.avatars__item {
            height: 49px ;
            margin: 0px 0px 0px 0px ;
            padding: 0px 0px 0px 0px ;
            
            width: 42px ; /* Forces flex items to be smaller than their contents. */
        }
        /*
            These zIndex values will only be relative to the contents of the UL element,
            which sets up its own stack container. As such, these will only be relevant
            to each other, not to the page at large.
            --
            NOTE: We could theoretically get around having to set explicit zIndex values
            by using "flex-direction: row-reverse" and using the natural stacking order
            of the DOM; however, to do that, we'd have to reverse the order of the HTML
            elements, which feels janky and unnatural.
        */
        li.avatars__item:nth-child( 1 ) { z-index: 9 ; }
        li.avatars__item:nth-child( 2 ) { z-index: 8 ; }
        li.avatars__item:nth-child( 3 ) { z-index: 7 ; }
        li.avatars__item:nth-child( 4 ) { z-index: 6 ; }
        li.avatars__item:nth-child( 5 ) { z-index: 5 ; }
        li.avatars__item:nth-child( 6 ) { z-index: 4 ; }
        li.avatars__item:nth-child( 7 ) { z-index: 3 ; }
        li.avatars__item:nth-child( 8 ) { z-index: 2 ; }
        li.avatars__item:nth-child( 9 ) { z-index: 1 ; }
        /*
            These items are all 49px wide while the flex-item (in which they live) is
            only 42px wide. As such, there will be several pixels of overflow content,
            which will be displayed in a reverse-stacking order based on above zIndex.
        */
        img.avatars__img,
        span.avatars__initials,
        span.avatars__others {
            background-color: #596376 ;
           
            border-radius: 100px 100px 100px 100px ;
            color: #FFFFFF ;
            
            font-family: sans-serif ;
            font-size: 12px ;
            font-weight: 100 ;
            height: 45px ;
            line-height: 45px ;
            text-align: center ;
            width: 45px ;
        }
        span.avatars__others {
            background-color: #1E8FE1 ;
        }
    </style>
    <section class="content">
      <div class="container-fluid">

       <div id="panel-1" class="panel">
          <div class="panel-hdr">
              <h2>
                Colaboradores

              </h2>
              <div class="panel-toolbar">
                 
              </div>
          </div>    
          <div class="panel-container show">
              <div class="panel-content">
                <div class="row">
                  <div class="col-md-3">
                    <label>UN</label>
                     <select id="unidad" class="form-control select2" onchange="filterColabora()">
                      <option value="Todos" selected>Todos</option>
                      <option value="STAFF">STAFF</option>
                      @foreach ($filtros['unidades'] as $un)
                          <option value="{{ $un['codigo_unidad'] }}">{{ $un['descripcion'] }}</option>    
                      @endforeach
                    </select>
                  </div>
                  <div class="col-md-3">
                    <label>Area</label>
                    <select id="area" class="form-control select2" onchange="filterColabora()">
                      <option value="0">Todas</option>
                        @foreach ($filtros['areas'] as $area)
                          <option value="{{ $area['id_area'] }}">{{ $area['nombre'] }}</option>    
                        @endforeach
                    </select>
                  </div>
                  <div class="col-md-3">
                    <label>Ingreso </label>
                    <select id="ingreso" class="form-control select2" onchange="filterColabora()">
                      <option value="0">-Selecciona-</option>
                      <option value="1">Menor a un año</option>
                      <option value="2">Entre uno y dos años</option>
                      <option value="3">Entre 2 y 5 años</option>
                      <option value="4">Entre 5  y 10</option>
                      <option value="5">Entre 10 y 15 años</option>
                      <option value="6">Mayor a 15 años</option>
                    </select>
                  </div>
                  <div class="col-md-3">
                    <label>Estatus</label>
                    <select id="estatus" class="form-control select2" onchange="filterColabora();">
                      <option value="3">Todos</option>
                      <option value="1" selected>Activo</option>
                      <option value="0">Baja</option>
                    </select>
                  </div>
                </div>
                <br>
                <div class="row">
                  <div class="col-md-12">
                    <table class="table table-bordered table-hover table-striped w-100" id="tableGrid">
                      <thead class="bg-warning-200">
                        <tr>
                          <th>ID</th>
                          <th>Nombre</th>
                          <th>Correo</th>
                          <th>Movil</th>
                          <th>Ingreso</th>
                          <th>UN</th>
                          <th>Area</th>
                          
                        </tr>
                      </thead>
                      <tbody>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
          </div> 
       </div>





       <!-- <div class="row">
          <div class="col-md-12">
            <div class="card card-primary card-outline">
              <div class="card-header">
                <div class="car-title">
                  Colaboradores
                </div>
              </div>
              <div class="card-body">
                <div class="row">
                  <div class="col-md-3">
                    <label>UN</label>
                     <select id="unidad" class="form-control select2" onchange="filterColabora()">
                      <option value="Todos" selected>Todos</option>
                      <option value="STAFF">STAFF</option>
                      @foreach ($filtros['unidades'] as $un)
                          <option value="{{ $un['codigo_unidad'] }}">{{ $un['descripcion'] }}</option>    
                      @endforeach
                    </select>
                  </div>
                  <div class="col-md-3">
                    <label>Area</label>
                    <select id="area" class="form-control select2" onchange="filterColabora()">
                      <option value="0">Todas</option>
                        @foreach ($filtros['areas'] as $area)
                          <option value="{{ $area['id_area'] }}">{{ $area['nombre'] }}</option>    
                        @endforeach
                    </select>
                  </div>
                  <div class="col-md-3">
                    <label>Ingreso </label>
                    <select id="ingreso" class="form-control select2" onchange="filterColabora()">
                      <option value="0">-Selecciona-</option>
                      <option value="1">Menor a un año</option>
                      <option value="2">Entre uno y dos años</option>
                      <option value="3">Entre 2 y 5 años</option>
                      <option value="4">Entre 5  y 10</option>
                      <option value="5">Entre 10 y 15 años</option>
                      <option value="6">Mayor a 15 años</option>
                    </select>
                  </div>
                  <div class="col-md-3">
                    <label>Estatus</label>
                    <select id="estatus" class="form-control select2" onchange="filterColabora();">
                      <option value="3">Todos</option>
                      <option value="1" selected>Activo</option>
                      <option value="0">Baja</option>
                    </select>
                  </div>
                </div>
                <br>
                <div class="row">
                  <div class="col-md-12">
                    <table class="table table-bordered table-hover table-striped w-100" id="tableGrid">
                      <thead class="bg-warning-200">
                        <tr>
                          <th>ID</th>
                          <th>Nombre</th>
                          <th>Correo</th>
                          <th>Movil</th>
                          <th>Ingreso</th>
                          <th>UN</th>
                          <th>Area</th>
                          
                        </tr>
                      </thead>
                      <tbody>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>-->


    
      </div><!-- /.container-fluid -->
    </section>

    <!-- /.content -->
@endsection

@section('scriptpage')
<!-- DataTable -->
<meta name="csrf-token" content="{{ csrf_token() }}">
<script>
            $.ajaxSetup({
            headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
          });
  $(document).ready(function() {
    
    //$('.select2').select2({ width: '100%',height:'100%' });
    $('#tableGrid').DataTable({
      //processing: true,
      //serverSide: true,
      responsive : true,
      ajax: "{{ route('api.getcolaboradores') }}",
      columns: [
        {data: 'id_colaborador', name: 'id_colaborador'},
        {data: 'nombre', name: 'nombre'},
        {data: 'correo_trabajo', name: 'correo_trabajo'},
        {data: 'telefono_movil', name: 'telefono_movil'},
        {data: 'ingreso_fecha', name: 'ingreso_fecha'},
        {data: 'un_imagen', name: 'un_imagen'},
        {data: 'area', name: 'area'},
        
      ],
      language: {
          "decimal": "",
          "emptyTable": "No hay información",
          "info": "Mostrando _START_ a _END_ de _TOTAL_ Entradas",
          "infoEmpty": "Mostrando 0 to 0 of 0 Entradas",
          "infoFiltered": "(Filtrado de _MAX_ total entradas)",
          "infoPostFix": "",
          "thousands": ",",
          "lengthMenu": "Mostrar _MENU_ Entradas",
          "loadingRecords": "Cargando...",
          "processing": "Procesando...",
          
          "zeroRecords": "Sin resultados encontrados",
          "paginate": {
              "first": "Primero",
              "last": "Ultimo",
              "next": "Siguiente",
              "previous": "Anterior"
          }
      },
      dom: "<'row mb-3'<'col-sm-12 col-md-6 d-flex align-items-center justify-content-start'f><'col-sm-12 col-md-6 d-flex align-items-center justify-content-end'B>>" +
                        "<'row'<'col-sm-12'tr>>" +
                        "<'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7'p>>",
      buttons: [
          {
              extend: 'colvis',
              text: '<i class="fas fa-columns"></i>&nbsp;Columnas',
              titleAttr: 'Mostrar/Ocultar Columnas',
              className: 'btn-outline-primary btn-sm mr-1'
          },
          {
              extend: 'pdfHtml5',
              text: '<i class="fas fa-file-pdf"></i>&nbsp; PDF',
              titleAttr: 'Generar PDF',
              className: 'btn-outline-danger btn-sm mr-1'
          },
          {
              extend: 'excelHtml5',
              text: '<i class="fas fa-file-excel"></i>&nbsp; Excel',
              titleAttr: 'Generar Excel',
              className: 'btn-outline-success btn-sm mr-1'
          },
          {
              extend: 'copyHtml5',
              text: '<i class="fas fa-copy"></i> &nbsp;Copiar',
              titleAttr: 'Copiar Tabla',
              className: 'btn-outline-primary btn-sm mr-1'
          },
          {
              extend: 'print',
              text: '<i class="fas fa-print"></i>&nbsp; Imprimir',
              titleAttr: 'Imprimir Tabla',
              className: 'btn-outline-primary btn-sm'
          },
          {
              text: '<a href="javascript:void(0);" class="btn btn-outline-primary btn-lg btn-icon rounded-circle hover-effect-dot waves-effect waves-themed" onclick="altaColabora()"><i class="fa fa-plus" aria-hidden="true"></i></a>',
              titleAttr: 'Alta de Colaborador',
          }

      ],
    }); 
  });
  function altaColabora(){
    
  }
  function filterColabora(){
  
    $("#tableGrid").dataTable().fnDestroy();
        $('#tableGrid').DataTable({
      processing: true,
      serverSide: true,
      responsive : true,
      ajax: {
        url : "{{ route('colaboraFiltro') }}",
        type : 'POST',
        data : {
            un : $('#unidad').val(),
            area : $('#area').val(),
            ingreso : $('#ingreso').val(),
            estatus : $('#estatus').val(),
        }
      },
      columns: [
        {data: 'id_colaborador', name: 'id_colaborador'},
        {data: 'nombre', name: 'nombre'},
        {data: 'correo_trabajo', name: 'correo_trabajo'},
        {data: 'telefono_movil', name: 'telefono_movil'},
        {data: 'ingreso_fecha', name: 'ingreso_fecha'},
        {data: 'un_imagen', name: 'un_imagen'},
        {data: 'area', name: 'area'},
        
      ],
      language: {
          "decimal": "",
          "emptyTable": "No hay información",
          "info": "Mostrando _START_ a _END_ de _TOTAL_ Entradas",
          "infoEmpty": "Mostrando 0 to 0 of 0 Entradas",
          "infoFiltered": "(Filtrado de _MAX_ total entradas)",
          "infoPostFix": "",
          "thousands": ",",
          "lengthMenu": "Mostrar _MENU_ Entradas",
          "loadingRecords": "Cargando...",
          "processing": "Procesando...",
          "zeroRecords": "Sin resultados encontrados",
          "paginate": {
              "first": "Primero",
              "last": "Ultimo",
              "next": "Siguiente",
              "previous": "Anterior"
          }
      },
      dom: "<'row mb-3'<'col-sm-12 col-md-6 d-flex align-items-center justify-content-start'f><'col-sm-12 col-md-6 d-flex align-items-center justify-content-end'B>>" +
                        "<'row'<'col-sm-12'tr>>" +
                        "<'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7'p>>", 
      buttons: [
          {
              extend: 'colvis',
              text: '<i class="fas fa-columns"></i>&nbsp;Columnas',
              titleAttr: 'Mostrar/Ocultar Columnas',
              className: 'btn-outline-primary btn-sm mr-1'
          },
          {
              extend: 'pdfHtml5',
              text: '<i class="fas fa-file-pdf"></i>&nbsp; PDF',
              titleAttr: 'Generar PDF',
              className: 'btn-outline-danger btn-sm mr-1'
          },
          {
              extend: 'excelHtml5',
              text: '<i class="fas fa-file-excel"></i>&nbsp; Excel',
              titleAttr: 'Generar Excel',
              className: 'btn-outline-success btn-sm mr-1'
          },
          {
              extend: 'copyHtml5',
              text: '<i class="fas fa-copy"></i> &nbsp;Copiar',
              titleAttr: 'Copiar Tabla',
              className: 'btn-outline-primary btn-sm mr-1'
          },
          {
              extend: 'print',
              text: '<i class="fas fa-print"></i>&nbsp; Imprimir',
              titleAttr: 'Imprimir Tabla',
              className: 'btn-outline-primary btn-sm'
          },
          {
              text: '<a href="javascript:void(0);" class="btn btn-outline-primary btn-lg btn-icon rounded-circle hover-effect-dot waves-effect waves-themed" onclick="newcol(2)"><i class="fa fa-plus" aria-hidden="true"></i></a>',
              titleAttr: 'Alta de Colaborador',
          }

      ],
    }); 
  }
</script>
@endsection