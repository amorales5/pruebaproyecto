@extends('layouts.admin')
@extends('rh.menu')
@section('content')
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h5 class="m-0 text-dark">Colaboradores</h5>
          </div>

          <div class="col-sm-6">
          </div>
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

            


    <!-- Main content -->
   <!-- <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-tittle">ddd</h3>
            </div>
            <div class="box-body">
              <div class="col-xs-6"></div>
            </div>
          </div>
        </div>
      </div>
    </section>-->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-12">
            <div class="card">
              <div class="card-header">
                <div class="car-title">
                  <!--<div class="row">

                      <div class="col-md-2"><button id="dataBtn" class="btn btn-info btn-block btn-sm act" onclick="changeTask('dataBtn');"><i class="fa fa-fw fa-user"></i> Datos</button></div>
                      <div class="col-md-2"><button id="metricsBtn" class="btn btn-info btn-block btn-outline-info btn-sm act" onclick="changeTask('metricsBtn');"><i class="fa fa-fw fa-line-chart"></i> Metricas</button></div>
                      <div class="col-md-2"><button id="fileBtn" class="btn btn-info btn-block btn-outline-info btn-sm act" onclick="changeTask('fileBtn');"><i class="fa fa-fw fa-folder"></i>Expediente</button></div>
                 
                        
                  </div> -->
                  <div class="row">
                    <div class="col-lg-6">
                    <div class="row" id="arre">
                      
                        <div class="col-md-3"><button id="dataBtn" class="btn btn-primary btn-block" onclick="changeTask('dataBtn','cPMY3Q');"><i class="fa fa-fw fa-user"></i> Datos</button></div>
                        <div class="col-md-3"><button id="metricsBtn" class="btn btn-outline-primary btn-block" onclick="loadView('metricsBtn');"><i class="fa fa-fw fa-line-chart"></i> Metricas</button></div>
                        <!--<div class="col-md-3"><button id="fileBtn" class="btn btn-info btn-block btn-outline-info btn-sm act" onclick="changeTask('fileBtn');"><i class="fa fa-fw fa-folder"></i>Expediente</button></div>
                        <div class="col-md-3"><button id="fileBtn" class="btn btn-info btn-block btn-outline-info btn-sm act" onclick="changeTask('fileBtn');"><i class="fa fa-fw fa-folder"></i>Reconocimientos</button></div> -->

                    </div>
                    </div>
                    <div class="col-lg-6">
                     <!-- <div class="row">
                        
                          <div class="col-md-3"><button id="fileBtn" class="btn btn-info btn-block btn-outline-info btn-sm act" onclick="changeTask('fileBtn');"><i class="fa fa-fw fa-folder"></i>Compensaciones</button></div>
                          <div class="col-md-3"><button id="metricsBtn" class="btn btn-info btn-block btn-outline-info btn-sm act" onclick="changeTask('metricsBtn');"><i class="fa fa-fw fa-line-chart"></i> Metricas</button></div>
                          <div class="col-md-3"><button id="fileBtn" class="btn btn-info btn-block btn-outline-info btn-sm act" onclick="changeTask('fileBtn');"><i class="fa fa-fw fa-folder"></i>Expediente</button></div>
                          <div class="col-md-3"><button id="fileBtn" class="btn btn-info btn-block btn-outline-info btn-sm act" onclick="changeTask('fileBtn');"><i class="fa fa-fw fa-folder"></i>Expediente</button></div>
                      </div> -->
                    </div>
                  </div>
                </div>
              </div>
              <div class="card-body">
                <div class="row">
                  <input type="hidden" id="idColaborador" value="<?php echo $id_colaborador; ?>">
                  <div class="col-md-12" id="appianContentDiv">
                      <?php 
                        //echo $id_colaborador;
                      ?> 
                      <appian-record-view recordTypeUrlStub="cPMY3Q" recordIdentifier="<?php echo $id_colaborador; ?>" recordViewUrlStub="summary"></appian-record-view>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
@endsection

@section('scriptpage')
<!-- DataTable -->
  <!-- This script loads the Appian Web components; change it to your Appian server's domain -->
  <meta name="csrf-token" content="{{ csrf_token() }}">
       <script src="https://mbgedev.appiancloud.com/suite/tempo/ui/sail-client/embeddedBootstrap.nocache.js" id="appianEmbedded"></script>
       <script>
            $.ajaxSetup({
            headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
          });
            window.addEventListener("click", () => {
              console.log("click!!!");
            });
         function changeTask(task,idappian){
            var idcolabora = $('#idColaborador').val();
            $("#divid").load(" #divid"); //Te refresca únicamente el DOM con el ID que le digas. Obligatorio el espacio después del load tal y como aparece
            
            ///$('.act').addClass('btn-outline-primary');
           // $('#'+task).removeClass('btn-outline-primary');
               $('#metricsBtn').removeClass('btn-primary')
          $('#metricsBtn').addClass('btn-outline-primary')
          $('#dataBtn').removeClass('btn-outline-primary');
          $('#dataBtn').addClass('btn-primary')

            $("#appianContentDiv").empty();
            
            $("#appianContentDiv").append('<appian-record-view recordTypeUrlStub="'+idappian+'" recordIdentifier="'+$('#idColaborador').val()+'" recordViewUrlStub="summary"></appian-record-view>')
            /*$('appian-record-view').attr({
              recordIdentifier: idcolabora,
              recordViewUrlStub: task
            }); */
           
            //$("#appianContentDiv").load(" #appianContentDiv");
         }
         function loadView(task){
           
           // $('#dataBtn').removeAttr('class')
           // $('#metricsBtn').removeAttr('class');
           // $('#dataBtn').attr('class', 'btn btn-primary-outline btn-block')
           // $('#metricsBtn').attr('class', 'btn btn-primary btn-block')




          $('#dataBtn').removeClass('btn-primary')
          $('#dataBtn').addClass('btn-outline-primary')
          $('#metricsBtn').removeClass('btn-outline-primary');
          $('#metricsBtn').addClass('btn-primary')
          

         // $('.act').addClass('btn-outline-primary');
            //$('#'+task).removeClass('btn-outline-primary');
          var idcolabora = $('#idColaborador').val();
          //alert('eees')
            $.ajax({
              url: "/getMetricas",
              type: 'POST',
              dataType: 'json',
              data: {idcolabora: idcolabora},
            })
            .done(function(data) {
              //console.log(data);
              //alert('dddd');
              $("#appianContentDiv").empty();
            
              $("#appianContentDiv").append(data.html)
               //$('#tableMetrics').DataTable();
      $("#metricsDiv").load("#appianContentDiv");

          $("#metricsDiv").load("#appianContentDiv");
            })
            .fail(function() {
              console.log("error");
            })
            .always(function() {
              console.log("complete");
            });
          
         }
         function seeMetric(id){
           
            $("#appianContentDiv").empty();
            
            $("#appianContentDiv").append('<appian-record-view recordTypeUrlStub="1qrd0A" recordIdentifier="'+id+'" recordViewUrlStub="summary"></appian-record-view>')
         }
         function newMetric(){
          var idcolabora = $('#idColaborador').val();
          //alert(idcolabora);
             $.ajax({
               url: '/newMetric',
               type: 'POST',
               dataType: 'json',
              data: {idcolabora: idcolabora},
             })
             .done(function(data) {
               console.log(data);
               //alert(data.html);
                $("#appianContentDiv").empty();
            
              $("#appianContentDiv").append('<appian-task taskId="'+data.html+'"></appian-task>')
             })
             .fail(function() {
               console.log("error");
             })
             .always(function() {
               console.log("complete");
             });
             
           }
       </script>

@endsection
