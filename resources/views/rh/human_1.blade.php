@extends('layouts.admin')
@extends('rh.menu')
@section('content')
    <!-- Content Header (Page header) -->

            


    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <!-- Small boxes (Stat box) -->
        <div class="row">
          <div class="col-lg-3 col-6">
            <div class="info-box">
              <span class="info-box-icon bg-info"><i class="fa fa-envelope-o"></i></span>

              <div class="info-box-content">
                <span class="info-box-text">Messages</span>
                <span class="info-box-number">1,410</span>
              </div>
              <!-- /.info-box-content -->
            </div>
          </div>
          <div class="col-lg-3 col-6">
            <div class="info-box">
              <span class="info-box-icon bg-info"><i class="fa fa-envelope-o"></i></span>

              <div class="info-box-content">
                <span class="info-box-text">Messages</span>
                <span class="info-box-number">1,410</span>
              </div>
              <!-- /.info-box-content -->
            </div>
          </div>
          <div class="col-lg-3 col-6">
            <div class="info-box">
              <span class="info-box-icon bg-info"><i class="fa fa-envelope-o"></i></span>

              <div class="info-box-content">
                <span class="info-box-text">Messages</span>
                <span class="info-box-number">1,410</span>
              </div>
              <!-- /.info-box-content -->
            </div>
          </div>
          <div class="col-lg-3 col-6">
            <div class="info-box">
              <span class="info-box-icon bg-info"><i class="fa fa-envelope-o"></i></span>

              <div class="info-box-content">
                <span class="info-box-text">Messages</span>
                <span class="info-box-number">1,410</span>
              </div>
              <!-- /.info-box-content -->
            </div>
          </div>
        </div>
        <br><br>
        <div class="row">
          <div class="col-md-1"></div>
          <div class="col-md-2">
            
          </div>
          <div class="col-md-2">
            <div class="hexagono2" style="float:left;" id="atraccionHex">
              <span class="hexagono-content">
                <i class="fa fa-search fa-3x" aria-hidden="true"></i>
                <br>
                <strong style="font-size: 70%;">Atraccion y Seleccion de talento</strong>
              </span>
            </div>
          </div>
          <div class="col-md-2">
            <a href="{{ action('ColaboradoresController@index', ['id' => 1]) }}">
            <div class="hexagono2" style="float:left;" id="colaboradorHex">
              <span class="hexagono-content">
                <i class="fa fa-users fa-3x" aria-hidden="true"></i>
                <br>
                <strong style="font-size: 70%;">Colaboradores</strong>
              </span>
            </div>
            </a>
          </div>
          <div class="col-md-2">
            <div class="hexagono2" style="float:left;" id="retencionHex">
              <span class="hexagono-content">
                <i class="fa fa-thumbs-up fa-3x" aria-hidden="true"></i>
                <br>
                <strong style="font-size: 70%;">Retencion de Talento</strong>
              </span>
            </div>
          </div>
          <div class="col-md-2">
          </div>
          <div class="col-md-1">
          </div>
          <!--<div class="col-sm-12">
            <div class="hexagono2" style="float:left;"></div>
            <div class="hexagono2" style="float:left;"></div>
            <div class="hexagono2" style="float:left;"></div>
            <div class="hexagono2" style="float:left;"></div>
            <div class="hexagono2" style="float:left;"></div>
          </div> -->
        </div>
        <br><br><br>
        <div class="row">
          <div class="col-md-2"></div>
          <div class="col-md-2">
            <div class="hexagono2" style="float:left;" id="compensacionesHex">
              <span class="hexagono-content">
                <i class="fa fa-balance-scale fa-3x" aria-hidden="true"></i>
                <br>
                <strong style="font-size: 70%;">Compensaciones y Beneficios</strong>
              </span>
            </div>
          </div>
          <div class="col-md-2">
            <div class="hexagono2" style="float:left;" id="desarrolloHex">
              <span class="hexagono-content">
                <i class="fa fa-line-chart fa-3x" aria-hidden="true"></i>
                <br>
                <strong style="font-size: 70%;">Desarrollo Organizacional</strong>
              </span>
            </div>
          </div>
          <div class="col-md-2">
            <div class="hexagono2" style="float:left;" id="relacionesHex">
              <span class="hexagono-content">
                <i class="fa fa-handshake-o fa-3x" aria-hidden="true"></i>
                <br>
                <strong style="font-size: 70%;">Relaciones Laborales</strong>
              </span>
            </div>
          </div>
          <div class="col-md-2">
            <div class="hexagono2" style="float:left;" id="saludHex">
              <span class="hexagono-content">
                <i class="fa fa-heartbeat fa-3x" aria-hidden="true"></i>
                <br>
                <strong style="font-size: 70%;">Salud y Seguridad</strong>
              </span>
            </div>
          </div>
          <div class="col-md-2"></div>
          <!--<div class="col-sm-12">
            <div class="hexagono2" style="float:left;"></div>
            <div class="hexagono2" style="float:left;"></div>
            <div class="hexagono2" style="float:left;"></div>
            <div class="hexagono2" style="float:left;"></div>
            <div class="hexagono2" style="float:left;"></div>
          </div> -->
        </div>
        <br><br><br>
     <!-- <div class="row">
          <div class="col-lg-3 col-6">
            <div class="small-box bg-info">
              <div class="inner">
                <h3>150</h3>
                <p>New Orders</p>
              </div>
              <div class="icon">
                <i class="ion ion-bag"></i>
              </div>
              <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
            </div>
          </div>
          <div class="col-lg-3 col-6">
            <div class="small-box bg-success">
              <div class="inner">
                <h3>53<sup style="font-size: 20px">%</sup></h3>
                <p>Bounce Rate</p>
              </div>
              <div class="icon">
                <i class="ion ion-stats-bars"></i>
              </div>
              <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
            </div>
          </div>
          <div class="col-lg-3 col-6">
            <div class="small-box bg-warning">
              <div class="inner">
                <h3>44</h3>
                <p>User Registrations</p>
              </div>
              <div class="icon">
                <i class="ion ion-person-add"></i>
              </div>
              <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
            </div>
          </div>
          <div class="col-lg-3 col-6">
            <div class="small-box bg-danger">
              <div class="inner">
                <h3>65</h3>
                <p>Unique Visitors</p>
              </div>
              <div class="icon">
                <i class="ion ion-pie-graph"></i>
              </div>
              <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
            </div>
          </div>
        </div> -->
        <!-- /.row (main row) -->
        <div class="row">
          <div class="col-lg-4 col-12">
                <!-- USERS LIST -->
                <div class="card card card-primary card-outline">
                  <div class="card-header">
                    <h3 class="card-title">Cumpleaños y Aniversarios</h3>

                    <div class="card-tools">
                      <!--<span class="badge badge-danger">8 New Members</span>-->
                      <button type="button" class="btn btn-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                      </button>
                      <button type="button" class="btn btn-tool" data-widget="remove"><i class="fa fa-times"></i>
                      </button>
                    </div>
                  </div>
                  <!-- /.card-header -->
                  <div class="card-body p-0">
                    <ul class="users-list clearfix">
                      <li>
                        <img src="../dist/img/user1-128x128.jpg" alt="User Image" class="imgCumple">
                        <a class="users-list-name" href="#">Alexander Pierce</a>
                        <span class="users-list-date">Today</span>
                      </li>
                      <li>
                        <img src="../dist/img/user8-128x128.jpg" alt="User Image" class="imgCumple">
                        <a class="users-list-name" href="#">Norman</a>
                        <span class="users-list-date">Yesterday</span>
                      </li>
                      <li>
                        <img src="../dist/img/user7-128x128.jpg" alt="User Image" class="imgCumple">
                        <a class="users-list-name" href="#">Jane</a>
                        <span class="users-list-date">12 Jan</span>
                      </li>
                      <li>
                        <img src="../dist/img/user6-128x128.jpg" alt="User Image" class="imgCumple">
                        <a class="users-list-name" href="#">John</a>
                        <span class="users-list-date">12 Jan</span>
                      </li>
                      <li>
                        <img src="../dist/img/user2-160x160.jpg" alt="User Image" class="imgCumple">
                        <a class="users-list-name" href="#">Alexander</a>
                        <span class="users-list-date">13 Jan</span>
                      </li>
                      <li>
                        <img src="../dist/img/user5-128x128.jpg" alt="User Image" class="imgCumple">
                        <a class="users-list-name" href="#">Sarah</a>
                        <span class="users-list-date">14 Jan</span>
                      </li>
                      <li>
                        <img src="../dist/img/user4-128x128.jpg" alt="User Image" class="imgCumple">
                        <a class="users-list-name" href="#">Nora</a>
                        <span class="users-list-date">15 Jan</span>
                      </li>
                      <li>
                        <img src="../dist/img/user3-128x128.jpg" alt="User Image" class="imgCumple">
                        <a class="users-list-name" href="#">Nadia</a>
                        <span class="users-list-date">15 Jan</span>
                      </li>
                    </ul>
                    <!-- /.users-list -->
                  </div>
                  <!-- /.card-body -->
                  <!--<div class="card-footer text-center">
                    <a href="javascript::">View All Users</a>
                  </div>-->
                  <!-- /.card-footer -->
                </div>            
          </div>
          <div class="col-lg-4 col-12">
            
            <div class="card card-primary card-outline">
              <div class="card-header">
                <h3 class="card-title">Striped Full Width Table</h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body p-0">
                <table class="table table-striped">
                  <tr>
                    <th style="width: 10px">#</th>
                    <th>Task</th>
                    <th>Progress</th>
                    <th style="width: 40px">Label</th>
                  </tr>
                  <tr>
                    <td>1.</td>
                    <td>Update software</td>
                    <td>
                      <div class="progress progress-xs">
                        <div class="progress-bar progress-bar-danger" style="width: 55%"></div>
                      </div>
                    </td>
                    <td><span class="badge bg-danger">55%</span></td>
                  </tr>
                  <tr>
                    <td>2.</td>
                    <td>Clean database</td>
                    <td>
                      <div class="progress progress-xs">
                        <div class="progress-bar bg-warning" style="width: 70%"></div>
                      </div>
                    </td>
                    <td><span class="badge bg-warning">70%</span></td>
                  </tr>
                  <tr>
                    <td>3.</td>
                    <td>Cron job running</td>
                    <td>
                      <div class="progress progress-xs progress-striped active">
                        <div class="progress-bar bg-primary" style="width: 30%"></div>
                      </div>
                    </td>
                    <td><span class="badge bg-primary">30%</span></td>
                  </tr>
                  <tr>
                    <td>4.</td>
                    <td>Fix and squish bugs</td>
                    <td>
                      <div class="progress progress-xs progress-striped active">
                        <div class="progress-bar bg-success" style="width: 90%"></div>
                      </div>
                    </td>
                    <td><span class="badge bg-success">90%</span></td>
                  </tr>
                </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <div class="col-lg-4 col-12">
<div class="card">
              <div class="card-header no-border">
                <h3 class="card-title">Frases del Dia</h3>
               <!-- <div class="card-tools">
                  <a href="#" class="btn btn-sm btn-tool">
                    <i class="fa fa-download"></i>
                  </a>
                  <a href="#" class="btn btn-sm btn-tool">
                    <i class="fa fa-bars"></i>
                  </a>
                </div>-->
              </div>
              <div class="card-body">
                <div class="row">
                  <div class="col-sm-3">
                    <img src="../dist/img/user7-128x128.jpg" alt="User Image" class="imgQuote">
                  </div>
                  <div class="col-sm-9">
                    <blockquote class="blockquote mb-0">
                      <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer posuere erat a ante.</p>
                      <footer class="blockquote-footer">Someone famous in <cite title="Source Title">Source Title</cite></footer>
                    </blockquote>
                  </div>
                </div>


              </div>
            </div>           
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
@endsection
