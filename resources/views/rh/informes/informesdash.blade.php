@extends('layouts.admin')
@extends('rh.menu')
@section('content')
    <!-- Content Header (Page header) -->

            

<section class="content-header">
	<h2>Informes</h2>
</section>
<!-- Main content -->
<section class="content">
  <div class="container-fluid">
	<div class="row">
      <div class="col-12 col-sm-6 col-md-3">
        <div class="info-box">
          <span class="info-box-icon bg-info elevation-1"><i class="fa fa-child"></i></span>

          <div class="info-box-content">
            <span class="info-box-text">Vacantes</span>
            <span class="info-box-number">
              10
              
            </span>
          </div>
          <!-- /.info-box-content -->
        </div>
        <!-- /.info-box -->
      </div>
      <!-- /.col -->
      <div class="col-12 col-sm-6 col-md-3">
      	<a href="{{ action('Informes\MetricasController@index') }}">
        <div class="info-box mb-3">
          <span class="info-box-icon bg-danger elevation-1"><i class="fa fa-area-chart"></i></span>

          <div class="info-box-content">
            <span class="info-box-text">Metrica General</span>
            <span class="info-box-number">96%</span>
          </div>

          <!-- /.info-box-content -->
        </div>
        </a>
        <!-- /.info-box -->
      </div>
      <!-- /.col -->

      <!-- fix for small devices only -->
      <div class="clearfix hidden-md-up"></div>

      <div class="col-12 col-sm-6 col-md-3">
        <div class="info-box mb-3">
          <span class="info-box-icon bg-success elevation-1"><i class="fa  fa-birthday-cake"></i></span>

          <div class="info-box-content">
            <span class="info-box-text">Cumpleaños del mes</span>
            <span class="info-box-number">7</span>
          </div>
          <!-- /.info-box-content -->
        </div>
        <!-- /.info-box -->
      </div>
      <!-- /.col -->
      <div class="col-12 col-sm-6 col-md-3">
        <div class="info-box mb-3">
          <span class="info-box-icon bg-warning elevation-1"><i class="fa fa-users"></i></span>

          <div class="info-box-content">
            <span class="info-box-text">Colaboradores</span>
            <span class="info-box-number">156</span>
          </div>
          <!-- /.info-box-content -->
        </div>
        <!-- /.info-box -->
      </div>
      <!-- /.col -->
    </div>
    <div class="row">
      <div class="col-md-4">
          <div class="card border  m-auto m-lg-0" style="width: 100%;">
              <div class="card-header">
                  <div style="float: left">
                    <i class="fa fa-heart fa-2x"></i>

                  </div>
                  <div style="float: left;">
                    <h3>&nbsp;Capital Humano</h3>
                  </div>
              </div>
              <ul class="list-group list-group-flush">
                  <li class="list-group-item"><a href="{{ action('Informes\MetricasController@index') }}"><i class="fa fa-umbrella"></i>&nbsp;Reporte de Vacaciones</a></li>
                  <li class="list-group-item"><a href="{{ action('Informes\MetricasController@index') }}"><i class="fa fa-area-chart"></i>&nbsp;Reporte de Inasistencias</a></li>
                  <li class="list-group-item"><a href="{{ action('Informes\MetricasController@index') }}"><i class="fa fa-area-chart"></i>&nbsp;Otros</a></li>
              </ul>
          </div>
      </div>
      <div class="col-md-4">
          <div class="card border  m-auto m-lg-0" style="width: 100%;">
              <div class="card-header">
                  <div style="float: left">
                    <i class="fa fa-area-chart fa-2x"></i>

                  </div>
                  <div style="float: left;">
                    <h3>&nbsp;Desarrollo Organizacional</h3>
                  </div>
              </div>
              <ul class="list-group list-group-flush">
                  <li class="list-group-item"><a href="{{ action('Informes\MetricasController@index') }}"><i class="fa fa-area-chart"></i>&nbsp;Metricas de Desempeño</a></li>
                  <li class="list-group-item"><a href="{{ action('Informes\MetricasController@index') }}"><i class="fa fa-line-chart"></i>&nbsp;Evaluaciones 30°</a></li>
				  <li class="list-group-item"><a href="{{ action('Informes\MetricasController@index') }}"><i class="fa fa-heartbeat"></i>&nbsp;Evaluaciones Clima Laboral</a></li>
              </ul>
          </div>        
      </div>
      <div class="col-md-4">
          <div class="card border  m-auto m-lg-0" style="width: 100%;">
              <div class="card-header">
                  Featured
              </div>
              <ul class="list-group list-group-flush">
                  <li class="list-group-item">Cras justo odio</li>
                  <li class="list-group-item">Dapibus ac facilisis in</li>
                  <li class="list-group-item">Vestibulum at eros</li>
              </ul>
          </div>        
      </div>
    </div>











  </div><!-- /.container-fluid -->
</section>
<!-- /.content -->
@endsection

@section('scriptpage')
<script src="{{ asset('plugins/chart.js/Chart.min.js') }}"></script>
<script src="{{ asset('dist/js/demo.js') }}"></script>
<script src="{{ asset('dist/js/pages/dashboard3.js') }}"></script>

@endsection