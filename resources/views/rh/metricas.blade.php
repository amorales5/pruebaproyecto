                <div class="row">
                  <div class="col-md-12" id="metricsDiv">
                   <!-- <table class="table table-bordered table-hover table-striped w-100" id="tableMetrics">
                      <thead class="bg-warning-200">
                        <tr>
                          <th>Estatus</th>
                          <th>Nombre</th>
                          <th>Inicio</th>
                          <th>Final</th>
                          <th>Resultado</th>
                        </tr>
                      </thead>
                      <tbody>
                      @foreach ($metricas as $un)
                          <tr>
                            <td>{{ $un['cerrada'] }}</td>
                            <td>{{ $un['nombre'] }}</td>
                            <td>{{ $un['fecha_ini'] }}</td>
                            <td>{{ $un['fecha_fin'] }}</td>
                            <td>{{ $un['resultado'] }}</td>
                          </tr>   
                      @endforeach
                      </tbody>
                    </table> -->
                    <table class="table table-bordered table-hover table-striped w-100" id="tableMetrics">
                      <thead class="bg-warning-200">
                        <tr>
                          <th>Estatus</th>
                          <th>Nombre</th>
                          <th>Inicio</th>
                          <th>Fin</th>
                          <th>Resultado</th>
                        </tr>
                      </thead>
                      <tbody>
                      @foreach ($metricas as $un)
                          <tr>
                            <td>{{ $un['cerrada'] }}</td>
                            <td>{{ $un['nombre'] }}</td>
                            <td>{{ $un['fecha_ini'] }}</td>
                            <td>{{ $un['fecha_fin'] }}</td>
                            <td>{{ $un['resultado'] }}</td>
                          </tr>   
                      @endforeach
                      </tbody>
                    </table>
                  </div>
                </div>
