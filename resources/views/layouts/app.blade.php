<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'SII') }}</title>

    <!-- Styles -->

        <link rel="stylesheet" media="screen, print" href="{{ asset('dist/css/vendors.bundle.css') }}">
        <link rel="stylesheet" media="screen, print" href="{{ asset('dist/css/app.bundle.css') }}">
        <!-- Place favicon.ico in the root directory -->
        <link rel="apple-touch-icon" sizes="180x180" href="{{ asset('dist/img/favicon/apple-touch-icon.png') }}">
      
        <link rel="mask-icon" href="{{ asset('dist/img/favicon/safari-pinned-tab.svg') }}" color="#5bbad5">
        <link rel="stylesheet" media="screen, print" href="{{ asset('dist/css/datagrid/datatables/datatables.bundle.css') }}">
        <link rel="stylesheet" media="screen, print" href="{{ asset('dist/css/theme-demo.css') }}">
        <script src="https://kit.fontawesome.com/de9ddc80e1.js" crossorigin="anonymous"></script>
    <!--<link href="{{ asset('css/app.css') }}" rel="stylesheet"> -->
    <style>
        body, html {
            height: 100%;
            background-repeat: no-repeat;
            background: url(https://image.freepik.com/vector-gratis/fondo-luminoso-puntos_1055-3132.jpg) no-repeat center center fixed;
            background-size: 100% 100%;
        }
        .card { background-color: rgba(245, 245, 245, 0.4); }
        .card-header, .card-footer { opacity: 1}
    </style>
</head>
<body>
   <!-- <div id="app">
        <nav class="navbar navbar-default navbar-static-top">
            <div class="container">
                <div class="navbar-header">

                    <!-- Collapsed Hamburger -->
       <!--             <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse" aria-expanded="false">
                        <span class="sr-only">Toggle Navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>

                    <!-- Branding Image -->
                   <!-- <a class="navbar-brand" href="{{ url('/') }}">
                        {{ config('app.name', 'Laravel') }}
                    </a> -->
               <!-- </div>

                <div class="collapse navbar-collapse" id="app-navbar-collapse">
                    <!-- Left Side Of Navbar -->
                <!--    <ul class="nav navbar-nav">
                        &nbsp;
                    </ul>

                    <!-- Right Side Of Navbar -->
                <!--    <ul class="nav navbar-nav navbar-right">
                        <!-- Authentication Links -->
                    <!--    @guest
                            <li><a href="{{ route('login') }}">Login</a></li>
                            <li><a href="{{ route('register') }}">Register</a></li>
                        @else
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false" aria-haspopup="true" v-pre>
                                    {{ Auth::user()->name }} <span class="caret"></span>
                                </a>

                                <ul class="dropdown-menu">
                                    <li>
                                        <a href="{{ route('logout') }}"
                                            onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                            Logout
                                        </a>

                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                        </form>
                                    </li>
                                </ul>
                            </li>
                        @endguest
                    </ul>
                </div>
            </div>
        </nav>-->

        @yield('content')
   <!-- </div>-->

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}"></script>
<script src="{{ asset('dist/js/vendors.bundle.js') }}"></script>
<script src="{{ asset('dist/js/app.bundle.js') }}"></script>
<script type="text/javascript">
    /* Activate smart panels */
    $('#js-page-content').smartPanel();

</script>
<!-- The order of scripts is irrelevant. Please check out the plugin pages for more details about these plugins below: -->
<script src="{{ asset('dist/js/statistics/peity/peity.bundle.js') }}"></script>
<script src="{{ asset('dist/js/statistics/flot/flot.bundle.js') }}"></script>
<script src="{{ asset('dist/js/statistics/easypiechart/easypiechart.bundle.js') }}"></script>
  <!--  <script src="{{ asset('js/app.js') }}"></script> -->
</body>
</html>
