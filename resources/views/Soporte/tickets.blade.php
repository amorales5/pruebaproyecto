@extends('layouts.admin')
@extends('Soporte.tema')
@extends('Soporte.menuCC')

@section('content')

    <section class="content">
                <ol class="breadcrumb page-breadcrumb">
                            <li class="breadcrumb-item"><a href="javascript:void(0);">SmartAdmin</a></li>
                            <li class="breadcrumb-item">Application Intel</li>
                            <li class="breadcrumb-item active">Tickets panel</li>
                            <li class="position-absolute pos-top pos-right d-none d-sm-block"><span class="js-get-date"></span></li>
                        </ol>
                        <div class="subheader">
                            <h1 class="subheader-title">
                                <i class='subheader-icon fal fa-chart-area'></i> <span class='fw-300'>Tickets</span>
                                <small>
                                </small>
                            </h1>
       </section>

    <section class="content">
    
    
      <div class="container-fluid">
       <div id="panel-1" class="panel">
          <div class="panel-hdr">
              <h2></h2>
              <div class="panel-toolbar">
                 
              </div>
          </div> 
                <div class="row">
                  <div class="col-md-12">
                    <table class="table table-bordered table-hover table-striped w-100" id="tableGrid">
                      <thead class="bg-warning-200">
                        <tr>
                          <th>ID</th>
                          <th>Cuenta</th>
                          <th>Título</th>
                          <th>Descripción</th>
                          <th>Fecha creación</th>
                          <th>Tipo</th>
                          <th>Prioridad</th>
                          <th>Asignado</th>                          
                        </tr>
                      </thead>
                      <tbody>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
          </div> 
       </div>    
      </div><!-- /.container-fluid -->
    </section>

@endsection


@section('scriptpage')
<!-- DataTable -->
<meta name="csrf-token" content="{{ csrf_token() }}">
<script>
            $.ajaxSetup({
            headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
          });
  $(document).ready(function() {
    
    //$('.select2').select2({ width: '100%',height:'100%' });
    $('#tableGrid').DataTable({
      //processing: true,
      //serverSide: true,
      responsive : true,
      ajax: "{{ route('api.gettickets') }}",
      columns: [
        {data: 'id_caso', name: 'id_caso'},
        {data: 'clave_cuenta', name: 'clave_cuenta'},
        {data: 'titulo', name: 'titulo'},
        {data: 'descripcion', name: 'descripcion'},
        {data: 'fecha_creacion', name: 'fecha_creacion'},
        {data: 'id_tipo', name: 'id_tipo'},
        {data: 'prioridad', name: 'prioridad'},
        {data: 'asignado_a', name: 'asignado_a'},

        
      ],
      language: {
          "decimal": "",
          "emptyTable": "No hay información",
          "info": "Mostrando _START_ a _END_ de _TOTAL_ Entradas",
          "infoEmpty": "Mostrando 0 to 0 of 0 Entradas",
          "infoFiltered": "(Filtrado de _MAX_ total entradas)",
          "infoPostFix": "",
          "thousands": ",",
          "lengthMenu": "Mostrar _MENU_ Entradas",
          "loadingRecords": "Cargando...",
          "processing": "Procesando...",
          
          "zeroRecords": "Sin resultados encontrados",
          "paginate": {
              "first": "Primero",
              "last": "Ultimo",
              "next": "Siguiente",
              "previous": "Anterior"
          }
      },
      dom: "<'row mb-3'<'col-sm-12 col-md-6 d-flex align-items-center justify-content-start'f><'col-sm-12 col-md-6 d-flex align-items-center justify-content-end'B>>" +
                        "<'row'<'col-sm-12'tr>>" +
                        "<'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7'p>>",
      buttons: [
          {
              extend: 'colvis',
              text: '<i class="fas fa-columns"></i>&nbsp;Columnas',
              titleAttr: 'Mostrar/Ocultar Columnas',
              className: 'btn-outline-primary btn-sm mr-1'
          },
          {
              extend: 'pdfHtml5',
              text: '<i class="fas fa-file-pdf"></i>&nbsp; PDF',
              titleAttr: 'Generar PDF',
              className: 'btn-outline-danger btn-sm mr-1'
          },
          {
              extend: 'excelHtml5',
              text: '<i class="fas fa-file-excel"></i>&nbsp; Excel',
              titleAttr: 'Generar Excel',
              className: 'btn-outline-success btn-sm mr-1'
          },
          {
              extend: 'copyHtml5',
              text: '<i class="fas fa-copy"></i> &nbsp;Copiar',
              titleAttr: 'Copiar Tabla',
              className: 'btn-outline-primary btn-sm mr-1'
          },
          {
              extend: 'print',
              text: '<i class="fas fa-print"></i>&nbsp; Imprimir',
              titleAttr: 'Imprimir Tabla',
              className: 'btn-outline-primary btn-sm'
          }

      ],
    }); 
  });
 
</script>
@endsection