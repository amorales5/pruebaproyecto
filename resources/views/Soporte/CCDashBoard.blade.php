@extends('layouts.admin')
@extends('Soporte.tema')
@extends('Soporte.menuCC')
@section('content')
                        <ol class="breadcrumb page-breadcrumb">
                            <li class="breadcrumb-item"><a href="javascript:void(0);">SmartAdmin</a></li>
                            <li class="breadcrumb-item">Application Intel</li>
                            <li class="breadcrumb-item active">Case managment Dashboard</li>
                            <li class="position-absolute pos-top pos-right d-none d-sm-block"><span class="js-get-date"></span></li>
                        </ol>
                        <div class="subheader">
                            <h1 class="subheader-title">
                                <i class='subheader-icon fal fa-chart-area'></i> Case Managment <span class='fw-300'>Dashboard</span>
                                <small>
                                </small>
                            </h1>
                            <div class="subheader-block d-lg-flex align-items-center">
                                <div class="d-inline-flex flex-column justify-content-center mr-3">
                                    <span class="fw-300 fs-xs d-block opacity-50">
                                        <small>EXPENSES</small>
                                    </span>
                                    <span class="fw-500 fs-xl d-block color-primary-500">
                                        $47,000
                                    </span>
                                </div>
                                <span class="sparklines hidden-lg-down" sparkType="bar" sparkBarColor="#886ab5" sparkHeight="32px" sparkBarWidth="5px" values="3,4,3,6,7,3,3,6,2,6,4"></span>
                            </div>
                            <div class="subheader-block d-lg-flex align-items-center border-faded border-right-0 border-top-0 border-bottom-0 ml-3 pl-3">
                                <div class="d-inline-flex flex-column justify-content-center mr-3">
                                    <span class="fw-300 fs-xs d-block opacity-50">
                                        <small>MY PROFITS</small>
                                    </span>
                                    <span class="fw-500 fs-xl d-block color-danger-500">
                                        $38,500
                                    </span>
                                </div>
                                <span class="sparklines hidden-lg-down" sparkType="bar" sparkBarColor="#fe6bb0" sparkHeight="32px" sparkBarWidth="5px" values="1,4,3,6,5,3,9,6,5,9,7"></span>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6 col-xl-3">
                                <div class="p-3 bg-primary-300 rounded overflow-hidden position-relative text-white mb-g">
                                    <div class="">
                                        <h3 class="display-4 d-block l-h-n m-0 fw-500">
                                            21
                                            <small class="m-0 l-h-n">Tickets Pendientes</small>
                                        </h3>
                                    </div>
                                    <i class="fal fa-user position-absolute pos-right pos-bottom opacity-15 mb-n1 mr-n1" style="font-size:6rem"></i>
                                </div>
                            </div>
                            <div class="col-sm-6 col-xl-3">
                                <div class="p-3 bg-warning-400 rounded overflow-hidden position-relative text-white mb-g">
                                    <div class="">
                                        <h3 class="display-4 d-block l-h-n m-0 fw-500">
                                            10
                                            <small class="m-0 l-h-n">Tickets Abiertos</small>
                                        </h3>
                                    </div>
                                    <i class="fal fa-gem position-absolute pos-right pos-bottom opacity-15  mb-n1 mr-n4" style="font-size: 6rem;"></i>
                                </div>
                            </div>
                            <div class="col-sm-6 col-xl-3">
                                <div class="p-3 bg-success-200 rounded overflow-hidden position-relative text-white mb-g">
                                    <div class="">
                                        <h3 class="display-4 d-block l-h-n m-0 fw-500">
                                            50
                                            <small class="m-0 l-h-n">Tickets en Espera</small>
                                        </h3>
                                    </div>
                                    <i class="fal fa-lightbulb position-absolute pos-right pos-bottom opacity-15 mb-n5 mr-n6" style="font-size: 8rem;"></i>
                                </div>
                            </div>
                            <div class="col-sm-6 col-xl-3">
                                <div class="p-3 bg-info-200 rounded overflow-hidden position-relative text-white mb-g">
                                    <div class="">
                                        <h3 class="display-4 d-block l-h-n m-0 fw-500">
                                            0
                                            <small class="m-0 l-h-n">Tickets no asignados</small>
                                        </h3>
                                    </div>
                                    <i class="fal fa-globe position-absolute pos-right pos-bottom opacity-15 mb-n1 mr-n4" style="font-size: 6rem;"></i>
                                </div>
                            </div>
                        </div>



                        <div id="panel-1" class="panel">
                            <div class="panel-hdr bg-primary-700 bg-success-gradient">
                                <h2>
                                    Line <span class="fw-300">Chart</span>
                                </h2>
                            </div>
                            <div class="panel-container show">
                                <div class="panel-content">
                                    <div class="panel-tag">
                                        The line chart requires an array of labels for each of the data points. This is shown on the X axis. It has a colour for the fill, a colour for the line and colours for the points and strokes of the points
                                    </div>
                                    <div id="areaChart">
                                        <canvas style="width:100%; height:300px;"></canvas>
                                    </div>
                                </div>
                            </div>
                        </div>





                        <div class="row">
							<div class="col-lg-12">
								                        	<div class="panel">
                                    <div class="panel-hdr bg-primary-700 bg-success-gradient">
                                        <h2>
                                            Panel <span class="fw-300">Colors</span>
                                        </h2>
                                    </div>
                                    <div class="panel-container show">
                                        <div class="panel-content">
                                            <div class="row">
                                            	<div class="col-md-5"><div class="card m-auto border">
                                                <div class="card-header py-2">
                                                    <div class="card-title">
                                                        Quote
                                                    </div>
                                                </div>
                                                <div class="card-body">

                                                </div>
                                            </div></div>
												<div class="col-md-7">
													<div class="card m-auto border">
                                                <div class="card-header py-2">
                                                    <div class="card-title">
                                                        SQL's
                                                    </div>
                                                </div>
                                                <div class="card-body">
													<div class="js-easy-pie-chart color-primary-300 position-relative d-inline-flex align-items-center justify-content-center" data-percent="100" data-piesize="95" data-linewidth="5" data-linecap="round" data-scalelength="7">
	                                                    <div class="d-flex flex-column align-items-center justify-content-center position-absolute pos-left pos-right pos-top pos-bottom fw-300 fs-xl">
	                                                        <span class="js-percent d-block text-dark"></span>
	                                                        <div class="d-block fs-xs text-dark opacity-70">
	                                                            <small>Memory</small>
	                                                        </div>
	                                                    </div>
	                                                </div>
													<div class="js-easy-pie-chart color-primary-300 position-relative d-inline-flex align-items-center justify-content-center" data-percent="86" data-piesize="95" data-linewidth="5" data-linecap="round" data-scalelength="7">
	                                                    <div class="d-flex flex-column align-items-center justify-content-center position-absolute pos-left pos-right pos-top pos-bottom fw-300 fs-xl">
	                                                        <span class="js-percent d-block text-dark"></span>
	                                                        <div class="d-block fs-xs text-dark opacity-70">
	                                                            <small>Memory</small>
	                                                        </div>
	                                                    </div>
	                                                </div>	 
													<div class="js-easy-pie-chart color-primary-300 position-relative d-inline-flex align-items-center justify-content-center" data-percent="86" data-piesize="95" data-linewidth="5" data-linecap="round" data-scalelength="7">
	                                                    <div class="d-flex flex-column align-items-center justify-content-center position-absolute pos-left pos-right pos-top pos-bottom fw-300 fs-xl">
	                                                        <span class="js-percent d-block text-dark"></span>
	                                                        <div class="d-block fs-xs text-dark opacity-70">
	                                                            <small>Memory</small>
	                                                        </div>
	                                                    </div>
	                                                </div>
													<div class="js-easy-pie-chart color-primary-300 position-relative d-inline-flex align-items-center justify-content-center" data-percent="86" data-piesize="95" data-linewidth="5" data-linecap="round" data-scalelength="7">
	                                                    <div class="d-flex flex-column align-items-center justify-content-center position-absolute pos-left pos-right pos-top pos-bottom fw-300 fs-xl">
	                                                        <span class="js-percent d-block text-dark"></span>
	                                                        <div class="d-block fs-xs text-dark opacity-70">
	                                                            <small>Memory</small>
	                                                        </div>
	                                                    </div>
	                                                </div>	
	                                            	<div class="js-easy-pie-chart color-primary-300 position-relative d-inline-flex align-items-center justify-content-center" data-percent="86" data-piesize="95" data-linewidth="5" data-linecap="round" data-scalelength="7">
	                                                    <div class="d-flex flex-column align-items-center justify-content-center position-absolute pos-left pos-right pos-top pos-bottom fw-300 fs-xl">
	                                                        <span class="js-percent d-block text-dark"></span>
	                                                        <div class="d-block fs-xs text-dark opacity-70">
	                                                            <small>Memory</small>
	                                                        </div>
	                                                    </div>
	                                                </div>                                                	                                	                                            	<div class="js-easy-pie-chart color-primary-300 position-relative d-inline-flex align-items-center justify-content-center" data-percent="86" data-piesize="95" data-linewidth="5" data-linecap="round" data-scalelength="7">
	                                                    <div class="d-flex flex-column align-items-center justify-content-center position-absolute pos-left pos-right pos-top pos-bottom fw-300 fs-xl">
	                                                        <span class="js-percent d-block text-dark"></span>
	                                                        <div class="d-block fs-xs text-dark opacity-70">
	                                                            <small>Memory</small>
	                                                        </div>
	                                                    </div>
	                                                </div>                                                               
                                                </div>
                                            </div>
												</div>							
                                            </div>
                                            <div class="row">
                                            	<div class="col-md-6">
                                            		<div class="row">
                                            			<div class="col-sm-12">
                                            				<h3>Campañas activas</h3>
                                            			</div>
                                            		</div>
                                            		<div class="row">
                                            			<div class="col-sm-12">
                                            				<table class="table m-0">
                                                    <thead>
                                                        <tr>
                                                            <th>#</th>
                                                            <th>First Name</th>
                                                            <th>Last Name</th>
                                                            <th>Username</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr>
                                                            <th scope="row">1</th>
                                                            <td>Mark</td>
                                                            <td>Otto</td>
                                                            <td>@mdo</td>
                                                        </tr>
                                                        <tr>
                                                            <th scope="row">2</th>
                                                            <td>Jacob</td>
                                                            <td>Thornton</td>
                                                            <td>@fat</td>
                                                        </tr>
                                                        <tr>
                                                            <th scope="row">3</th>
                                                            <td>Larry</td>
                                                            <td>the Bird</td>
                                                            <td>@twitter</td>
                                                        </tr>
                                                        <tr>
                                                            <th scope="row">4</th>
                                                            <td colspan="2">Larry the Bird</td>
                                                            <td>@twitter</td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            			</div>
                                            		</div>
                                            	</div>
                                            	<div class="col-md-6">
                                            		<div class="row">
                                            			<div class="col-sm-12">
                                            				<h3>Bases de Datos</h3>
                                            			</div>
                                            		</div>
                                            		<div class="row">
                                            			<div class="col-sm-12">
                                            				<table class="table m-0">
                                                    <thead>
                                                        <tr>
                                                            <th>#</th>
                                                            <th>First Name</th>
                                                            <th>Last Name</th>
                                                            <th>Username</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr>
                                                            <th scope="row">1</th>
                                                            <td>Mark</td>
                                                            <td>Otto</td>
                                                            <td>@mdo</td>
                                                        </tr>
                                                        <tr>
                                                            <th scope="row">2</th>
                                                            <td>Jacob</td>
                                                            <td>Thornton</td>
                                                            <td>@fat</td>
                                                        </tr>
                                                        <tr>
                                                            <th scope="row">3</th>
                                                            <td>Larry</td>
                                                            <td>the Bird</td>
                                                            <td>@twitter</td>
                                                        </tr>
                                                        <tr>
                                                            <th scope="row">4</th>
                                                            <td colspan="2">Larry the Bird</td>
                                                            <td>@twitter</td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            			</div>
                                            		</div>
                                            	</div>
                                            </div>
                                        </div>
                                    </div>
                            </div>
							</div>
                        </div>
@endsection





@section('scriptpage')
    <script>
    /* area chart */
        var areaChart = function()
        {
            var config = {
                type: 'line',
                data:
                {
                    labels: ["January", "February", "March", "April", "May", "June", "July"],
                    datasets: [
                    {
                        label: "Primary",
                        backgroundColor: 'rgba(136,106,181, 0.2)',
                        borderColor: myapp_get_color.primary_500,
                        pointBackgroundColor: myapp_get_color.primary_700,
                        pointBorderColor: 'rgba(0, 0, 0, 100)',
                        pointBorderWidth: 1,
                        borderWidth: 1,
                        pointRadius: 3,
                        pointHoverRadius: 4,
                        data: [45,75,26,23,60,-48,-9],
                        fill: true
                    },
                    {
                        label: "Success",
                        backgroundColor: 'rgba(29,201,183, 0.2)',
                        borderColor: myapp_get_color.success_500,
                        pointBackgroundColor: myapp_get_color.success_700,
                        pointBorderColor: 'rgba(0, 0, 0, 0)',
                        pointBorderWidth: 1,
                        borderWidth: 1,
                        pointRadius: 3,
                        pointHoverRadius: 4,
                        data: [-10,16,72,93,29,-74,64],
                        fill: true
                    },
                    {
                        label: "Success",
                        backgroundColor: 'rgba(10,0,0,0.3)',
                        borderColor: myapp_get_color.success_500,
                        pointBackgroundColor: myapp_get_color.success_700,
                        pointBorderColor: 'rgba(255, 255, 255, 255)',
                        pointBorderWidth: 1,
                        borderWidth: 1,
                        pointRadius: 3,
                        pointHoverRadius: 4,
                        data: [10,-10,10,-10,10,-10,10],
                        fill: true
                    }
                ]
                },
                options:
                {
                    responsive: true,
                    title:
                    {
                        display: false,
                        text: 'Area Chart'
                    },
                    tooltips:
                    {
                        mode: 'index',
                        intersect: false,
                    },
                    hover:
                    {
                        mode: 'nearest',
                        intersect: true
                    },
                    scales:
                    {
                        xAxes: [
                        {
                            display: true,
                            scaleLabel:
                            {
                                display: false,
                                labelString: '6 months forecast'
                            },
                            gridLines:
                            {
                                display: true,
                                color: "#f2f2f2"
                            },
                            ticks:
                            {
                                beginAtZero: true,
                                fontSize: 11
                            }
                        }],
                        yAxes: [
                        {
                            display: true,
                            scaleLabel:
                            {
                                display: false,
                                labelString: 'Profit margin (approx)'
                            },
                            gridLines:
                            {
                                display: true,
                                color: "#f2f2f2"
                            },
                            ticks:
                            {
                                beginAtZero: true,
                                fontSize: 11
                            }
                        }]
                    }
                }
            };
            new Chart($("#areaChart > canvas").get(0).getContext("2d"), config);
        }
        /* area chart -- end */
      
        /* initialize all charts */
        $(document).ready(function()
        {
            areaChart();
        });

    </script>

@endsection