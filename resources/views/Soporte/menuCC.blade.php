@section('menusCC')
<ul id="js-nav-menu" class="nav-menu">
    <li>
      <a href="#" title="Dashboard" data-filter-tags="application intel">
          <i class="fas fa-tachometer-alt" aria-hidden="true"></i>
          <span class="nav-link-text" data-i18n="nav.application_intel">Dashboard</span>
      </a>
    </li>
    <li>
      <a href="{{ action('Soporte\TestSoporteController@tickets') }}" title="Tickets" data-filter-tags="application intel">
          <i class="fas fa-cog" aria-hidden="true"></i>
          <span class="nav-link-text" data-i18n="nav.application_intel">Tickets</span>
      </a>
    </li>
    <li>
      <a href="#" title="Proyectos" data-filter-tags="application intel">
          <i class="fas fa-tasks" aria-hidden="true"></i>
          <span class="nav-link-text" data-i18n="nav.application_intel">Proyectos</span>
      </a>
    </li>
    <li>
      <a href="#" title="Reportes" data-filter-tags="application intel">
          <i class="fal fa-window-alt" aria-hidden="true"></i>
          <span class="nav-link-text" data-i18n="nav.application_intel">Reportes</span>
      </a>
    </li>
    <li>
      <a href="#" title="Tasks" data-filter-tags="application intel">
          <i class="fas fa-tasks" aria-hidden="true"></i>
          <span class="nav-link-text" data-i18n="nav.application_intel">My Tasks</span>
      </a>
    </li>                                                                     
</ul>
@endsection