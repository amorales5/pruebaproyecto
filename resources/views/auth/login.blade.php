@extends('layouts.app')

@section('content')
<div class="container" align="center">
    <br><br><br>
    <div class="col-lg-3"></div>
    <div class="col-lg-6">
    
        <div class="card">
          
            <div class="card-block" style="background-color:#FFFFFF;">
                <div class="col-md-12">
                        <div class="login-box">

                      <!-- /.login-logo -->
                      <div class="login-box-body">
                        <p class="login-box-msg">
                            <br><br>
                            <img src="{{ asset('dist/img/siiimages/SII-02.png') }}" alt="" style="width: 170px;height: 100px;">
                        </p>
                    <form class="form-horizontal" method="POST" action="{{ route('login') }}">
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <!--<label for="email" class="col-md-4 control-label">E-Mail Address</label>-->

                            <div class="col-md-12">
                                <input id="email" type="text" class="form-control" name="email" value="{{ old('email') }}" required autofocus placeholder="Usuario">

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <!--<label for="password" class="col-md-4 control-label">Password</label> -->

                            <div class="col-md-12">
                                <input id="password" type="password" class="form-control" name="password" required placeholder="Contraseña">

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-12">
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> Recuerdame
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-12">
                                <button type="submit" class="btn btn-primary">
                                    Login
                                </button>

                                <a class="btn btn-link" href="{{ route('password.request') }}">
                                    <br><br>
                                </a>
                            </div>
                        </div>
                    </form>
                       <!-- <div class="row">
                            <div class="col-sm-6"><a href="#">I forgot my password</a></div>
                            <div class="col-sm-6"><a href="register.html" class="text-center">Register a new membership</a></div>
                        </div>-->
                       <!-- <a href="#">I forgot my password</a><br>
                        <a href="register.html" class="text-center">Register a new membership</a> -->

                      </div>
  <!-- /.login-box-body -->
</div>
                </div>
            </div>
        </div>
    </div>
<!-- /.login-box -->
<div class="col-lg-3"></div>
</div>
@endsection
