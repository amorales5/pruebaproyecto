<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/redi/{id}', 'RedirectController@index') ->where('id', '[0-9]+')->name('redi');
//Route::get('/redi/{id}', 'RedirectController@index') ->where('id', '[0-9]+')->name('redi');

/*Route::group(['middleware' => 'web', 'namespace' => 'App\Http\Controllers'], function () {
    Route::get('/password-reset', 'Auth\ResetPasswordController@showResetForm')
        ->name('cognito.password-reset');
});*/
Route::get('/test', 'TestController@index')->name('test');
Route::get('api.colaboradores','ColaboradoresController@index')->name('api.colaboradores');
Route::get('api.getcolaboradores','ColaboradoresController@getColaboradores')->name('api.getcolaboradores');
Route::post('/getMetricas', 'ColaboradoresController@getMetricas')->name('getMetricas');
;
Route::post('/colaboraFiltro', 'ColaboradoresController@colaboraFiltro')->name('colaboraFiltro');

Route::resource('empleados','EmpleadosController');
Route::get('/metricas','Informes\MetricasController@index')->name('metricas');
Route::post('/newMetric', 'Informes\MetricasController@nuevaMetrica')->name('nuevaMetrica');
Route::get('/metricasone/{id}', 'Informes\MetricasController@show') ->where('id', '[0-9]+')->name('show');
Route::get('/informes','Informes\MetricasController@informesIndex')->name('informesIndex');
Route::post('/metricasFiltro', 'Informes\MetricasController@metricasFiltro')->name('metricasFiltro');

///Chat
Route::get('/contacts', 'ContactsController@get');
Route::get('/conversation/{id}', 'ContactsController@getMessagesFor');
Route::post('/conversation/send/', 'ContactsController@send');



//CaseManagement
Route::get('api.CCDashBoard','Soporte\TestSoporteController@index')->name('api.CCDashBoard');
Route::get('api.tickets','Soporte\TestSoporteController@tickets')->name('api.tickets');
Route::get('api.gettickets','Soporte\TestSoporteController@getTickets')->name('api.gettickets');
//Route::get('/soporte','Soporte/TestSoporteController@tickets')->name('tickets');
